<?php
/*
Plugin Name: Super Yeti
Plugin URI: http://yetipass.ch
Description: Plugin de parrainage Super Yeti
Version: 1.0
Author: MEO design & communications (Kyle Mobilia)
Author URI: http://meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

// Load Autoloader Composer
require_once( $plugin_root . '/models/YetiPassRefundModel.php' );
require_once( $plugin_root . '/models/SuperYetiUserModel.php' );
require_once( $plugin_root . '/classe/SuperYeti.php' );
require_once( $plugin_root . '/ajax/ajax_super_yeti.php' );

# Defines / Constants
define('MEO_SUPER_YETI_USER_TABLE', 'meo_super_yeti_user');
define('MEO_YETI_PASS_REFUND_TABLE', 'meo_yeti_pass_refund');

session_start();

# Plugin activation
function meo_super_yeti_active () {
    global $wpdb;

    $installed_dependencies = false;
    if ( is_plugin_active( 'woocommerce/woocommerce.php' )) {
        $installed_dependencies = true;
    }

    if(!$installed_dependencies) {

        // WordPress check for fatal error while activating plugin, so simplest solution will be trigger a fatal error
        // and this will prevent WordPress to activate the plugin.
        echo '<div class="notice notice-error"><h3>'.__('Please install and activate Woocommerce plugins before', 'meo-realestate').'</h3></div>';

        //Adding @ before will prevent XDebug output
        @trigger_error(__('Please install and activate Woocommerce plugins before.', 'meo-realestate'), E_USER_ERROR);
        exit;

    }else{

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $charset_collate = $wpdb->get_charset_collate();

        # SUPER YETI USER TABLE
        $table_name = MEO_SUPER_YETI_USER_TABLE;
        $sql = "CREATE TABLE $table_name (
		           id INTEGER(11) NOT NULL AUTO_INCREMENT,
		           user_id INTEGER(11) NOT NULL,
		           code VARCHAR(255) NULL,
		           updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		           created_at DATETIME DEFAULT NULL,
		           PRIMARY KEY (id)
					) $charset_collate;";
        dbDelta( $sql );

        # YETI PASS REFUND TABLE
        $table_name = MEO_YETI_PASS_REFUND_TABLE;
        $sql = "CREATE TABLE $table_name (
	            id INTEGER(11) NOT NULL AUTO_INCREMENT,
	            super_yeti_id INTEGER(11),
	            got_date DATETIME,
	            refund_date DATETIME,
	            refund INTEGER(11),
	            PRIMARY KEY (id)
					) $charset_collate;";
        dbDelta( $sql );

    }
}
register_activation_hook( __FILE__, 'meo_super_yeti_active' );

# Add Scripts and Styles

# Admin

add_action( 'admin_enqueue_scripts', 'meo_super_yeti_scripts_styles_admin' );
function meo_super_yeti_scripts_styles_admin() {

    # JS
    wp_register_script( 'meo_super_yeti_view_js',  plugins_url('js/admin-super-yeti-script.js', __FILE__), false, '1.0.0', true );
    wp_enqueue_script( 'meo_super_yeti_view_js' );

    # CSS
    wp_register_style( 'meo_super_yeti_view_css',  plugins_url('css/admin-super-yeti-style.css', __FILE__), false, '1.0.0' );
    wp_enqueue_style( 'meo_super_yeti_view_css' );

}

# Front

add_action( 'wp_enqueue_scripts', 'meo_super_yeti_scripts_styles_front' );
function meo_super_yeti_scripts_styles_front() {

    # JS
    wp_register_script( 'meo_super_yeti_gift_js',  plugins_url('js/gift.js', __FILE__), false, '1.0.0', true );
    wp_enqueue_script( 'meo_super_yeti_gift_js' );

    wp_localize_script( 'meo_super_yeti_gift_js', 'meo_super_yeti_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

    wp_register_script( 'meo_super_yeti_popup_js',  plugins_url('js/popup.js', __FILE__), false, '1.0.0', true );
    wp_enqueue_script( 'meo_super_yeti_popup_js' );

    # CSS
    wp_register_style( 'meo_super_yeti_gift_css',   plugins_url('css/gift.css', __FILE__), false, '1.0.0' );
    wp_enqueue_style( 'meo_super_yeti_gift_css' );

    wp_register_style( 'meo_super_yeti_popup_css',   plugins_url('css/popup.css', __FILE__), false, '1.0.0' );
    wp_enqueue_style( 'meo_super_yeti_popup_css' );

    wp_register_style( 'meo_super_yeti_account_css',   plugins_url('css/myaccount.css', __FILE__), false, '1.0.0' );
    wp_enqueue_style( 'meo_super_yeti_account_css' );
}

/*
 * ADD CUSTOM POST TYPE GIFTS
 */
function create_post_types_gift() {
    // Post Type
    $labels = array(
        'name' => 'Cadeaux',
        'all_items' => 'Tous les cadeaux',
        'singular_name' => 'Event',
        'add_new_item' => 'Ajouter un cadeau',
        'edit_item' => "Modifier le cadeau",
        'menu_name' => 'Cadeaux'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-tickets-alt',
    );
    register_post_type('gift',$args);
}
add_action('init', 'create_post_types_gift');


/*
 * ADD FIELD SUPER YETI CODE IN CHECKOUT FORM
 */
function addSuperYetiCodeFieldToCheckout() {
    if(isset($_SESSION['superYetiCode']) && !empty($_SESSION['superYetiCode'])) {
        return SuperYeti::addSuperYetiCodeField($_SESSION['superYetiCode']);
    }else{
        return SuperYeti::addSuperYetiCodeField();
    }
}
add_action('woocommerce_checkout_shipping', 'addSuperYetiCodeFieldToCheckout');

/*
 * ADD GIFTS CHOOSE SUPER YETI IN CHECKOUT FORM
 */
function addGiftsChooseToCheckout() {
    SuperYeti::addGiftChooseRadio();
}
add_action('woocommerce_review_order_before_payment', 'addGiftsChooseToCheckout');

/*
 * SHOW SUPER YETI POPUP THANK YOU PAGE
 */
function popupSuperYetiThankYou() {
    SuperYeti::showPopupSuperYetiCode();
}
add_action('woocommerce_checkout_popup_yeti','popupSuperYetiThankYou');

/*
 * SHOW BUTTON OPEN SUPER YETI POPUP THANK YOU PAGE
 */
function buttonOpenPopupSuperYetiThankYou() {
    SuperYeti::buttonOpenPopupSuperYetiCode();
}
add_action('woocommerce_checkout_popup_yeti_open','buttonOpenPopupSuperYetiThankYou');

/*
 * CHECKOUT SUBMIT
 */
function customise_checkout_field_update_order_meta($order_id)
{
    global $wpdb;

    $code = SuperYeti::generateSuperYetiCode();
    $results = !empty($_POST['superyeti']) ? SuperYetiUserModel::getSuperYetiCodeBySuperYetiCode($_POST['superyeti']) : array();
    if (!empty($_POST['superyeti']) && !empty($results)) {
        update_post_meta($order_id, 'super_yeti', sanitize_text_field($_POST['superyeti']));
    }else if(!empty($code)){
        update_post_meta($order_id, 'super_yeti', sanitize_text_field($code));
    }
    if (!empty($_POST['choose_gift']) && !empty($_POST['superyeti'] && !empty($results))) {
        update_post_meta($order_id, 'gift_choose', sanitize_text_field($_POST['choose_gift']));
    }

}
add_action('woocommerce_checkout_update_order_meta', 'customise_checkout_field_update_order_meta');


function order_completed($order_id) {

    global $wpdb;

    $query  = 'SELECT * ';
    $query .= 'FROM '.$wpdb->prefix.'postmeta ';
    $query .= 'WHERE meta_key LIKE "super_yeti"';
    $query .= 'AND post_id = '.$order_id.'';
    $results = $wpdb->get_results($query);
    $superYetiCode = $results[0]->meta_value;

    $results = SuperYetiUserModel::getSuperYetiCodeBySuperYetiCode($superYetiCode);
    $user = get_user_by('id', $results[0]->user_id);

    $quantity = SuperYeti::getDownlineCountBySuperYetiCode($superYetiCode);
    $list = SuperYeti::getDownlineNameBySuperYetiCode($superYetiCode);

    /* CHECK IF REFUND SUPER YETI */

    $id_super_yeti_user = $results[0]->id;

    $refund_results = YetiPassRefundModel::getYetiPassRefund($id_super_yeti_user);

    $quantity_refund = 10*count(YetiPassRefundModel::getYetiPassRefund($id_super_yeti_user));
    $quantity_solde = $quantity-$quantity_refund;

    if($quantity_solde >= 10) {
        $number = explode('.', ($quantity_solde/10));
        for($i=0; $i < $number[0]; $i++) {
            YetiPassRefundModel::setYetiPassRefund($id_super_yeti_user);
        }
        $to_email = $user->user_email;
        $headers = array('From: Super Yeti <no-reply@yetipass.meomeo.ch>','Content-Type: text/html; charset=UTF-8');
        wp_mail($to_email, 'Vous êtes un Super Yeti', SuperYeti::templateEmailSuperYetiRefunded(), $headers );
    }

    wp_mail($to_email, 'Vous avez un nouveau YETIPASS', SuperYeti::templateEmailSuperYeti($quantity,$list), $headers );

}
add_action( 'woocommerce_thankyou', 'order_completed' );

function shareSuperYetiCodeFacebook(){
    if ( is_user_logged_in() ) {
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user_id);
        $superYetiCode = $results[0]->code;
        echo '<meta property="og:type"          content="website" />
              <meta property="og:title"         content="Your Website Title" />
              <meta property="og:description"   content="Your description" />';
    }
}
//add_action( 'additional_header_metadata', 'shareSuperYetiCodeFacebook' );


function getSuperYetiCodeSession() {
    $superYetiCode = (isset($_GET['yeticode']) && !empty($_GET['yeticode'])) ? $_GET['yeticode'] : '';
    if(!empty($superYetiCode)) {
        $_SESSION['superYetiCode'] = $superYetiCode;
    }
}
add_action( 'additional_header_metadata', 'getSuperYetiCodeSession' );

function customMyAccount() {

    $data = SuperYeti::getDataSuperYetiCode();

    $quantity = SuperYeti::getDownlineCountBySuperYetiCode($data['super_yeti_code']);
    $list = SuperYeti::getDownlineNameBySuperYetiCode($data['super_yeti_code']);

    echo '<p>'.__('[:fr]Votre code de parrainage est le :[:]').' <span class="orange-txt">'.$data['super_yeti_code'].'</span></p>';
    echo '<p>'.__('[:fr]Actuellement, il vous reste à trouver '.$quantity.' filleul/s/es YETIPASS ![:]').'</p>';
    echo '<h3>'.__('[:fr]VOS YETIFILLEUL/S/ES SONT[:]').'</h3>';
    echo '<ol>';
    foreach($list as $godson){
        echo  '<li>'.$godson.'</li>';
    }
    echo '</ol>';



}
add_action('woocommerce_account_dashboard','customMyAccount');

function customMyAccountShare() {

    $data = SuperYeti::getDataSuperYetiCode();

    echo '<div class="shareSuperYetiCode">';
    echo    '<p>'.__('[:fr]partager votre code superyeti sur[:]').'</p>';
    echo    '<ul>';
    echo        '<li><a onclick="window.open(\''.$data['link_facebook'].'\', \'newwindow\', \'width=600,height=350\'); return false;" href="'.$data['link_facebook'].'"><p><i class="fa fa-facebook-official" aria-hidden="true"></i><span>Facebook</span></p></a></li>';
    echo        '<li><a onclick="window.open(\''.$data['link_twitter'].'\', \'newwindow\', \'width=600,height=350\'); return false;" href="'.$data['link_twitter'].'"><p><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></p></a></li>';
    echo        '<li><a href="mailto:?subject=Cet hiver, passons plus de temps dans les montagnes enneigées&body='.$data['email_message'].'"><p><i class="fa fa-envelope" aria-hidden="true"></i><span>Email</span></p></a></li>';
    echo    '</ul>';
    echo '</div>';

}
add_action('woocommerce_account_super_yeti','customMyAccountShare');


add_action('admin_menu', 'test_plugin_setup_menu');

function test_plugin_setup_menu(){
    add_menu_page( 'Super Yeti', 'Super Yeti', 'manage_options', 'super-yeti', 'view_godson_user_init', 'dashicons-id-alt');
}

function view_godson_user_init(){
    include 'admin_template/template_super_yeti.php';
}

function showGiftOnOrderAdmin($order) {
    $metas = get_post_meta($order->id);
    echo '<br/>';
    if(isset($metas['gift_choose'][0]) && !empty($metas['super_yeti'][0])) {
        echo '<p"><strong>Super Yeti Code :</strong><br/>' . $metas['super_yeti'][0] . '</p>';
    }
    if(isset($metas['gift_choose'][0]) && !empty($metas['gift_choose'][0])) {
        echo '<p"><strong>Cadeau choisi :</strong><br/>'.$metas['gift_choose'][0].'</p>';
    }
}
add_action('woocommerce_admin_order_data_after_order_details','showGiftOnOrderAdmin');
















