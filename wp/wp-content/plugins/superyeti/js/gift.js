/**
 * Created by kylemobilia on 24.10.17.
 */

$(document).ready(function(){
    checkIfSuperYetiCodeIsTrue($('input#superyeti').val());
});

$('input#superyeti').keyup(function(){
    checkIfSuperYetiCodeIsTrue($(this).val());
});

$('input#superyeti').mouseout(function(){
    checkIfSuperYetiCodeIsTrue($(this).val());
});

$('input#superyeti').change(function(){
    checkIfSuperYetiCodeIsTrue($(this).val());
});

function checkIfSuperYetiCodeIsTrue(superYetiCode) {
    jQuery.ajax({
        url : 'http://'+window.location.hostname+'/wp/wp-admin/admin-ajax.php',
        type : 'post',
        data : {
            action: "checkIfSuperYetiCodeIsTrue",
            superYetiCode: superYetiCode
        },
        success : function( response ) {
            if(response) {
                $('#gifts').show();
            }else{
                $('#gifts').hide();
            }
        }
    });
}