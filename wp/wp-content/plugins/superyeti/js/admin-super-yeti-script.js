/**
 * Created by kylemobilia on 03.11.17.
 */

jQuery('#search-user-super-yeti').click(function(){
    jQuery.ajax({
        url : 'http://'+window.location.hostname+'/wp/wp-admin/admin-ajax.php',
        type : 'post',
        data : {
            action: "getUserSeahSuperYetiPageAdmin",
            search: jQuery('#search-user').val()
        },
        success : function( response ) {
            jQuery('#user-list-data tbody').html(response);
        }
    });
});

function submitFormRefund(user_id){
    var count = 0;
    var refundElements = {};
    jQuery('input[name="refund-check"]:checked').each(function(){
        refundElements['id_refund_'+count] = jQuery(this).attr('id');
        count++;
    });
    var json = JSON.stringify(refundElements);
    jQuery.ajax({
        url : 'http://'+window.location.hostname+'/wp/wp-admin/admin-ajax.php',
        type : 'post',
        data : {
            action: "getRefundUser",
            user_id: user_id,
            json: json
        },
        success : function( response ) {
            console.log(response);
            jQuery('ol.refund-list').html(response);
        }
    });
}

function openPopupDetailUser(id) {
    jQuery.ajax({
        url : 'http://'+window.location.hostname+'/wp/wp-admin/admin-ajax.php',
        type : 'post',
        data : {
            action: "getPopupUserDetailsSuperYeti",
            user_id: id
        },
        success : function( response ) {
            jQuery('.popup-user-details .top-popup-data').html(response);
            jQuery('div.popup_show_user').css('display','flex');
        }
    });
}
function closePopupUSerDetails() {
    jQuery('div.popup_show_user').css('display','none');
}