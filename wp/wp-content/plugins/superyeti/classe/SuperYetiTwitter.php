<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 26.10.17
 * Time: 11:26
 */

use Abraham\TwitterOAuth\TwitterOAuth;

class SuperYetiTwitter
{
    public $connection = ''; //Provide your application consumer key
    public $consumer_key = ''; //Provide your application consumer key
    public $consumer_secret = ''; //Provide your application consumer secret
    public $oauth_token = ''; //Provide your oAuth Token
    public $oauth_token_secret = ''; //Provide your oAuth Token Secret

    function __construct($consumer_key,$consumer_secret,$oauth_token = '',$oauth_token_secret = '')
    {
        $this->consumer_key = $consumer_key;
        $this->consumer_secret = $consumer_secret;
        $this->oauth_token = $oauth_token;
        $this->oauth_token_secret = $oauth_token_secret;

        $this->connection = new TwitterOAuth($this->consumer_key, $this->consumer_secret, $this->oauth_token, $this->oauth_token_secret);
    }

    public function authTwitterUser() {

        $callback_url = "http://".$_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"];

        // request token of application
        $request_token = $this->connection->oauth(
            'oauth/request_token'
        );

        // throw exception if something gone wrong
        if($this->connection->getLastHttpCode() != 200) {
            throw new \Exception('There was a problem performing this request');
        }

        // save token of application to session
        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

        // generate the URL to make request to authorize our application
        $url = $this->connection->url(
            'oauth/authorize',
                ['oauth_token' => $request_token['oauth_token']
            ]
        );

        return $url;
    }

    public function postOneTweetSingleText($message) {
        $this->connection->post('statuses/update', array('status' => $message));
    }

    public function postOneTweetTextAndMedia($message,$media) {
        $media1 = $this->connection->upload('media/upload', ['media' => $media]);
        $parameters = [
            'status' => $message,
            'media_ids' => implode(',', [$media1->media_id_string]),
        ];
        $this->connection->post('statuses/update', $parameters);
    }
}