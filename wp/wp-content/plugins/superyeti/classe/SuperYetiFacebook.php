<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 26.10.17
 * Time: 11:26
 */

use Facebook\Facebook;
use Facebook\GraphUser;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

class SuperYetiFacebook
{
    public $facebook = '';
    public $appId = '';
    public $appSecret = '';

    function __construct($appId,$appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->facebook = new Facebook([
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.10'
        ]);
    }

    function getLogin(){
        $callback = "http://".$_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"];
        $helper = $this->facebook->getRedirectLoginHelper();
        $permissions = ['email','publish_actions']; // Optional permissions
        $loginUrl = $helper->getLoginUrl($callback, $permissions);
        return htmlspecialchars($loginUrl);
    }

    function getTokenAccess(){

        $helper = $this->facebook->getCanvasHelper();
        return $helper->getAccessToken();
    }

    function callbackLogin(){
        $helper = $this->facebook->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(FacebookResponseException $e) {

        } catch(FacebookSDKException $e) {

        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {

            } else {

            }
        }else{

            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $this->facebook->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);

            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId($this->appId); // Replace {app-id} with your app id
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();

            if (! $accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (FacebookSDKException $e) {

                }
            }

            $_SESSION['fb_access_token'] = (string) $accessToken;

        }
    }

    function publishPost(){
        $linkData = [
            'link' => "http://".$_SERVER['SERVER_NAME'],
            'message' => 'User provided message',
        ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $this->facebook->post('/me/feed', $linkData, $_SESSION['fb_access_token']);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {

        } catch(Facebook\Exceptions\FacebookSDKException $e) {

        }

        $graphNode = $response->getGraphNode();

        return $graphNode['id'];
    }

}