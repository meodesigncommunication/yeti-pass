<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 23.10.17
 * Time: 20:24
 */

class SuperYeti
{
    private static $numberOfYetipassForFree = 10;
    private static $itemSponsored = 'Yetipass (Adulte)';

    /*
     * Generate Super Yeti Code
     */
    public static function generateSuperYetiCode(){
        $user = wp_get_current_user();
        if(!empty($user->ID)) {
            $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user->ID);
            if(!empty($results)){
                $super_yeti_code = $results[0]->code;
            }else{
                $code = 'YETI' . strtoupper($user->user_login) . $user->ID;
            }
            if(empty($super_yeti_code)) {
                if(SuperYetiUserModel::setSuperYetiUser($user->ID,$code)) {
                    return $code;
                }
            }else{
                return $super_yeti_code;
            }
        }
        return '';
    }

    /*
     * Add Super Yeti CodeField In Checkout Form
     */
    public static function addSuperYetiCodeField($superYetiCode = ''){
        echo '  <p class="form-row form-row-last" id="billing_superyeti_field">
                    <label for="superyeti" class="">Code Super Yeti</label>
                    <input class="input-text" name="superyeti" id="superyeti" placeholder="" value="'.$superYetiCode.'" type="text">
                </p>';
    }

    /*
     * Show Popup In Thank You Page
     */
    public static function showPopupSuperYetiCode() {
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $plugin_root = plugin_dir_url( __FILE__ );
        $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user_id);
        $superYetiCode = $results[0]->code;
        $url = 'http://'.$_SERVER['SERVER_NAME'].'/?yeticode='.$superYetiCode;
        $text = urlencode('YETIPASS à CHF 99.- = accès illimité au domaine de la Braye - hiver 17/18. Tarif enfant : 10.- ! Avec mon code '.$superYetiCode.', profitez de l’un des 3 bonus. Voyez par vous-même ');
        $message =  'Chers Amis,%0D%0A%0D%0ALe YETIPASS à CHF 99.- c’est un accès illimité au domaine de la Braye cette saison 17/18. %0D%0A%0D%0ATarif enfants : 10.- !%0D%0A%0D%0ALa Braye, c’est le domaine des skieurs et des non-skieurs, il y en a pour tous les goûts.%0D%0A%0D%0AVoyez par vous-même '.$url.'.%0D%0A%0D%0AAvec mon code YETI, profitez de l’un des 3 avantages offerts à l’achat d’un YETIPASS.%0D%0A'.$superYetiCode.'%0D%0A%0D%0AA bientôt';
        //$message = urlencode(static::templateEmailSuperYetiShare($superYetiCode,$url));
        $link_twitter = 'https://twitter.com/share?url='.urlencode($url).'&via=YETIPASS.ch&related=twitterapi%2Ctwitter&hashtags=YETIPASS&text='.$text;
        $link_facebook = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($url).'&amp;src=sdkpreparse';

        echo '  <div id="popup-super-yeti">                
                <div class="popup">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <div class="col-1"><img class="superyeti" src="'.$plugin_root.'../images/superyeti.png" alt="'.__('[:fr]Deviens un SuperYeti[:]').'"></div>
                    <div class="col-2">
                        <span>'.__('[:fr]Merci pour votre achat[:]').'</span>
                        <span>'.__('[:fr]Devenez Super-Yeti !<br/><strong>Invitez vos amis</strong>[:]').'</span>
                        <span>'.__('[:fr]A l’achat d’un 10ème Yetipass par vos amis<br/><strong>votre abonnement vous est remboursé*</strong>[:]').'</span>
                        <div class="social-network-partage">
                            <a onclick="window.open(\''.$link_facebook.'\', \'newwindow\', \'width=600,height=350\'); return false;" href="'.$link_facebook.'"><button>'.__('[:fr]Partagez sur Facebook[:]').'</button></a>
                            <a onclick="window.open(\''.$link_twitter.'\', \'newwindow\', \'width=600,height=350\'); return false;" href="'.$link_twitter.'"><button>'.__('[:fr]Partagez sur Twitter[:]').'</button></a>
                            <a href="mailto:?subject=Cet hiver, passons plus de temps dans les montagnes enneigées&body='.$message.'"><button>'.__('[:fr]Partagez par email[:]').'</button></a>
                        </div>
                        <div class="super-yeti-code">'.__('[:fr]Votre code Super-Yeti[:]').': '.$superYetiCode.'</div>
                        <span>'.__('[:fr]*Vos amis qui utiliseront votre code super-yeti pour leurs achats<br/>recevront un cadeau spécial du yeti | <a href="'.home_url('/').'/conditions-generales" title="">voir la liste des cadeaux et des conditions</a>[:]').'</span>
                    </div>
                </div>
            </div>';
    }

    /*
     * Get data Super Yeti
     */
    public static function getDataSuperYetiCode() {
        $data_return = array();
        $user = wp_get_current_user();
        $user_id = $user->ID;
        $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user_id);
        $data_return['super_yeti_code'] = $superYetiCode = $results[0]->code;
        $url = $_SERVER['SERVER_NAME'].'%2F?yeticode='.$superYetiCode;
        $url = 'http://'.$_SERVER['SERVER_NAME'].'/?yeticode='.$superYetiCode;
        $text = urlencode('YETIPASS à CHF 99.- = accès illimité au domaine de la Braye - hiver 17/18. Tarif enfant : 10.- ! Avec mon code '.$superYetiCode.', profitez de l’un des 3 bonus. Voyez par vous-même ');
        $data_return['email_message'] = 'Chers Amis,%0D%0A%0D%0ALe YETIPASS à CHF 99.- c’est un accès illimité au domaine de la Braye cette saison 17/18. %0D%0A%0D%0ATarif enfants : 10.- !%0D%0A%0D%0ALa Braye, c’est le domaine des skieurs et des non-skieurs, il y en a pour tous les goûts.%0D%0A%0D%0AVoyez par vous-même '.$url.'.%0D%0A%0D%0AAvec mon code YETI, profitez de l’un des 3 avantages offerts à l’achat d’un YETIPASS.%0D%0A'.$superYetiCode.'%0D%0A%0D%0AA bientôt';
        //$data_return['email_message'] = urlencode(static::templateEmailSuperYetiShare($superYetiCode,$url));
        $data_return['link_twitter'] = 'https://twitter.com/share?url='.urlencode($url).'&via=YETIPASS.ch&related=twitterapi%2Ctwitter&hashtags=YETIPASS&text='.$text;
        $data_return['link_facebook'] = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($url).'&amp;src=sdkpreparse';

        return $data_return;
    }

    /*
     * Button For Open Popup In Thank You Page
     */
    public static function buttonOpenPopupSuperYetiCode() {
        echo '<button id="open-popup-super-yeti-code">'.__('[:fr]OUVRIR FENÊTRE SUPERYETI[:]').'</button>';
    }

    /*
     * Show Gifts List In Checout Form
     */
    public static function addGiftChooseRadio() {
        $count = 0;
        $args = array(
            'post_type' => 'gift',
            'posts_per_page' => -1,
            'order_by' => 'id',
            'order' => 'ASC'
        );
        $gifts = query_posts($args);
        $html = '<div id="gifts" class="woocommerce-gifts">
                <h3>'.__('[:fr]CHOISISSEZ VOTRE CADEAU ![:]').'</h3>
                <ul class="wc_gifts">';
        foreach($gifts as $gift) {
            if($count == 0)
            {
                $checked = 'checked="checked"';
            }else{
                $checked = '';
            }
            $html .= '<li class="">';
            $html .=    '<input id="'.$gift->post_name.'" class="input-radio" name="choose_gift" value="'.$gift->post_title.'" ' . $checked . ' data-order_button_text="" type="radio">';
            $html .=    '<label for="choose_gift">'.$gift->post_title.'</label>';
            $html .= '</li>';
            $count++;
        }
        $html .= '</ul>
            </div>';

        echo $html;
    }

    public static function getDownlineCountBySuperYetiCode($superYetiCode) {
        global $wpdb;
        $quantity = 0;
        $orders = array();

        // GET ORDER ID SPONSORED BY SUPER YETI CODE
        $query  = 'SELECT * ';
        $query .= 'FROM '.$wpdb->prefix.'postmeta ';
        $query .= 'WHERE meta_value = "'.$superYetiCode.'"';
        $results_postmeta = $wpdb->get_results($query);
        foreach($results_postmeta as $result_postmeta) {
            // GET ORDER ITEM ID SPONSORED BY SUPER YETI CODE FOR CHECK THE QUANTITY
            $query  = 'SELECT * ';
            $query .= 'FROM '.$wpdb->prefix.'woocommerce_order_items ';
            $query .= 'WHERE order_id = "'.$result_postmeta->post_id.'"';
            $results_order_item = $wpdb->get_results($query);
            foreach($results_order_item as $result_order_item) {
                if($result_order_item->order_item_name == static::$itemSponsored) {
                    // GET ORDER ITEM META _QTY SPONSORED BY SUPER YETI CODE
                    $query  = 'SELECT * ';
                    $query .= 'FROM '.$wpdb->prefix.'woocommerce_order_itemmeta ';
                    $query .= 'WHERE meta_key = "_qty" ';
                    $query .= 'AND order_item_id = '.$result_order_item->order_item_id.'';
                    $results_order_item_meta = $wpdb->get_results($query);
                    $quantity += $results_order_item_meta[0]->meta_value;
                }
            }
        }
        return $quantity;
    }

    public static function getDownlineNameBySuperYetiCode($superYetiCode) {
        global $wpdb;
        $list = array();
        $orders = array();

        // GET ORDER ID SPONSORED BY SUPER YETI CODE
        $query  = 'SELECT * ';
        $query .= 'FROM '.$wpdb->prefix.'postmeta ';
        $query .= 'WHERE meta_value = "'.$superYetiCode.'"';
        $results_postmeta = $wpdb->get_results($query);
        foreach($results_postmeta as $result_postmeta) {
            // GET ORDER ITEM ID SPONSORED BY SUPER YETI CODE FOR CHECK THE QUANTITY
            $query  = 'SELECT * ';
            $query .= 'FROM '.$wpdb->prefix.'woocommerce_order_items ';
            $query .= 'WHERE order_id = "'.$result_postmeta->post_id.'"';
            $results_order_item = $wpdb->get_results($query);
            foreach($results_order_item as $result_order_item) {
                if($result_order_item->order_item_name == static::$itemSponsored) {
                    // GET ORDER ITEM META _QTY SPONSORED BY SUPER YETI CODE
                    $query  = 'SELECT * ';
                    $query .= 'FROM '.$wpdb->prefix.'woocommerce_order_itemmeta ';
                    $query .= 'WHERE meta_key LIKE "%. Nom complet%" ';
                    $query .= 'AND order_item_id = '.$result_order_item->order_item_id.'';
                    $results_order_item_meta = $wpdb->get_results($query);
                    foreach($results_order_item_meta as $result_order_item_meta) {
                        $list[] = $result_order_item_meta->meta_value;
                    }
                }
            }
        }
        return $list;
    }

    public static function templateEmailSuperYeti($quantity,$list) {

        $godson_to_stay = 10-($quantity%10);


        $html = '  <html>
                    <head>
                        <style>                            
                        </style>
                    </head>
                    <body style="background-color: #f7f7f7;">
                        <div style="width: 550px; margin: 50px auto; font-family: \'Gotham-Light\'; font-weight: lighter; border-radius: 2px; overflow: hidden;">
                            <div style="background-color: #cd4017;color: #FFF; padding: 50px; font-size: 26px; text-transform: uppercase;">
                                Vous avez un nouveau YETIPASS
                            </div>
                            <div style="border: 1px solid #a0a0a0; border-top: 0; padding: 50px; color: #646265; font-size: 14px; font-family: \'Gotham-Book\'; background-color: #fff;">
                            <p>
                                Cher, Chère Superyeti,
                                <br/><br/>
                                Nous sommes heureux de vous informer que vous avez un nouveau filleul !
                                <br/><br/>
                                Plus que '.$godson_to_stay.' et votre abonnement vous sera remboursé.
                                <br/><br/>
                                Avec nos meilleures salutations.
                                <br/><br/>
                                L’équipe de Télé-Château-d’Oex
                                <br/><br/>
                                <span style="color:#cd4017">LISTE DE VOS FILLEULS:</span>
                                </p>
                                    <ul>';
                                    foreach($list as $godson){
                                        $html .=  '<li>'.$godson.'</li>';
                                    }
                                    $html .= '</ul>
                            </div>
                        </div>
                    </body>
                </html>';

        return $html;
    }

    public static function templateEmailSuperYetiRefunded() {

        $html = '  <html>
                    <head>
                        <style>                            
                        </style>
                    </head>
                    <body style="background-color: #f7f7f7;">
                        <div style="width: 550px; margin: 50px auto; font-family: \'Gotham-Light\'; font-weight: lighter; border-radius: 2px; overflow: hidden;">
                            <div style="background-color: #cd4017;color: #FFF; padding: 50px; font-size: 26px; text-transform: uppercase;">
                                Vous êtes un Super Yeti
                            </div>
                            <div style="border: 1px solid #a0a0a0; border-top: 0; padding: 50px; color: #646265; font-size: 14px; font-family: \'Gotham-Book\'; background-color: #fff;">
                            <p>
                                Félicitations ! Vous êtes….. un superyeti !
                                <br/><br/>
                                Grace à vous, vos 9 filleuls sont les heureux détenteurs d’un yetipass à un prix incroyable et d’un avantage client. Et vous, d’un yetipass gratuit !
                                <br/><br/>
                                Faite vous rembourser les CHF 99.- de votre abonnement de saison 17-18 au guichet de Télé-Château-d’Oex lors du retrait de votre Yetipass.
                                <br/><br/>
                                Nous serons heureux de vous y accueillir dès le 4 décembre 2017.
                                <br/><br/>
                                L’équipe de Télé-Château-d\'Oex
                                </p>
                            </div>
                        </div>
                    </body>
                </html>';

        return $html;
    }

    public static function templateEmailSuperYetiShare($superYetiCode,$url) {

        $html = '%3Cp%3EChers%20Amis%2C%3Cbr%2F%3E%3Cbr%2F%3EJe%20vous%20glisse%20un%20lien%20sur%20l%E2%80%99offre%20d%E2%80%99abonnement%20de%20saison%202017%2F2018%20du%20domaine%20de%20La%20Braye%20%C3%A0%20Ch%C3%A2teau-d%E2%80%99Oex.%3Cbr%2F%3E%3Cbr%2F%3EAllez%20voir%20par%20vous-m%C3%AAme%20sur%20le%20site%20%3Ca%20href%3D%22%27.%24url.%27%22%3Eyetipass.ch%3C%2Fa%3E%3Cbr%2F%3E%3Cbr%2F%3ELe%20code%20%C3%A0%20utiliser%3Cbr%2F%3E%27.%24superYetiCode.%27%3Cbr%2F%3E%3Cbr%2F%3EMoi%2C%20j%E2%80%99ai%20mon%20yetipass%21%3Cbr%2F%3E%3Cbr%2F%3EA%20bient%C3%B4t%3C%2Fp%3Ep';

        return $html;
    }

    public static function sendEmailWithCountToSponsored($code){

        $results = SuperYetiUserModel::getSuperYetiCodeBySuperYetiCode($code);
        $user_id = $results[0]->user_id;

        $user_info = get_userdata($user_id);

        $to = $user_info->user_email;
        $subject = 'Vous vous rapprochez du status de Super-Yeti';
        $body = 'text';

        if(wp_mail( $to, $subject, $body )) {
            return true;
        }

        return false;
    }

}