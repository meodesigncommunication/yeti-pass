<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 03.11.17
 * Time: 09:35
 */
?>
<div class="wrap">
    <h1 class="wp-heading-inline">SUPER YETI PARRAIN</h1>
    <div class="search-user">
        <input type="text" id="search-user" name="search-user" placeholder="Rechercher un client" />
        <button id="search-user-super-yeti"><span class="dashicons dashicons-search"></span></button>
    </div>
    <div class="results-user">
        <table id="user-list-data">
            <thead>
                <tr>
                    <th>#ID User</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Email</th>
                    <th>Super Yeti Code</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <!--<tr>
                <td>#45</td>
                <td>Test</td>
                <td>Test</td>
                <td>test@test.com</td>
                <td>YETITEST45</td>
                <td><span class="dashicons dashicons-visibility"></span></td>
            </tr>-->
        </table>
    </div>
    <div class="popup_show_user">
        <div class="popup-user-details">
            <div class="top-popup-data">

            </div>
        </div>
    </div>
</div>
