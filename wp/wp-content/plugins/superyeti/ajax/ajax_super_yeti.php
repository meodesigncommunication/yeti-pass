<?php
/**
 * Created by PhpStorm.
 * User: kylemobilia
 * Date: 24.10.17
 * Time: 02:36
 */

function checkIfSuperYetiCodeIsTrue() {

    $superYetiCode = $_POST['superYetiCode'];

    $results = SuperYetiUserModel::getSuperYetiCodeBySuperYetiCode($superYetiCode);

    if(!empty($results)) {
        echo true;
    }else{
        echo false;
    }

    die();
}
add_action( 'wp_ajax_checkIfSuperYetiCodeIsTrue', 'checkIfSuperYetiCodeIsTrue' );
add_action( 'wp_ajax_nopriv_checkIfSuperYetiCodeIsTrue', 'checkIfSuperYetiCodeIsTrue' );

function getUserSeahSuperYetiPageAdmin() {
    global $wpdb;

    $html = '';
    $search = $_POST['search'];

    $query  = 'SELECT u.ID, spu.code ';
    $query .= 'FROM '.$wpdb->prefix.'users AS u ';
    $query .= 'LEFT JOIN '.$wpdb->prefix.'usermeta AS um ON u.ID = um.user_id ';
    $query .= 'LEFT JOIN meo_super_yeti_user AS spu ON u.ID = spu.user_id ';
    $query .= 'WHERE (um.meta_key = "first_name" AND um.meta_value LIKE "%'.$search.'%") ';
    $query .= 'OR (um.meta_key = "last_name" AND um.meta_value LIKE "%'.$search.'%") ';
    $query .= 'OR (u.ID = "'.$search.'") ';
    $query .= 'OR (u.user_login LIKE "%'.$search.'%") ';
    $query .= 'OR (u.display_name LIKE "%'.$search.'%") ';
    $query .= 'OR (u.user_email LIKE "%'.$search.'%") ';
    $query .= 'OR (spu.code LIKE "%'.$search.'%") ';
    $query .= 'GROUP BY u.ID ';

    $results = $wpdb->get_results($query);

    foreach($results as $result) {

        $user = get_userdata($result->ID);

        $html .= '<tr>';
        $html .=    '<td>#'.$result->ID.'</td>';
        $html .=    '<td>'.$user->last_name.'</td>';
        $html .=    '<td>'.$user->first_name.'</td>';
        $html .=    '<td>'.$user->user_email.'</td>';
        $html .=    '<td>'.$result->code.'</td>';
        $html .=    '<td><span onclick="openPopupDetailUser('.$result->ID.')" id="'.$result->ID.'" class="dashicons dashicons-visibility"></span></td>';
        $html .= '</tr>';

    }

    echo $html;

    die();
}
add_action( 'wp_ajax_getUserSeahSuperYetiPageAdmin', 'getUserSeahSuperYetiPageAdmin' );
add_action( 'wp_ajax_nopriv_getUserSeahSuperYetiPageAdmin', 'getUserSeahSuperYetiPageAdmin' );

function getPopupUserDetailsSuperYeti() {

    $user_id = $_POST['user_id'];
    $list_html = '';
    $user = get_userdata($user_id);
    $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user_id);
    $superYetiCode = $results[0]->code;

    $refund_results = YetiPassRefundModel::getYetiPassRefund($results[0]->id);

    $quantity = SuperYeti::getDownlineCountBySuperYetiCode($superYetiCode);
    $quantity = 10-($quantity%10);

    $list = SuperYeti::getDownlineNameBySuperYetiCode($superYetiCode);

    foreach($list as $key => $element) {
        $list_html .= '<li>'.$element.'</li>';
    }
    $list_refund_html = '';
    foreach($refund_results as $result_refund) {
        $date_got = new DateTime($result_refund->got_date);
        if($result_refund->refund){
            $date_refund = new DateTime($result_refund->refund_date);
            $list_refund_html .= '<li><span class="dashicons dashicons-yes"></span><label>Obtenu le '.$date_got->format('d/m/Y').', remboursé le '.$date_refund->format('d/m/Y').'</label></li>';
        }else{
            $list_refund_html .= '<li><input type="checkbox" id="'.$result_refund->id.'" name="refund-check" value="1"><label>Obtenu le '.$date_got->format('d/m/Y').'</label></li>';
        }
    }

    $html = '<div class="wrapper">
                    <p class="personal-data">'.$user->last_name.' '.$user->first_name.' - '.$user->user_email.'</p>
                    <p class="code-data">'.$superYetiCode.'</p>
                    <p>
                        Il reste <span id="quantity-godson">'.$quantity.'</span> YETIPASS à parrainer avant d\'être remboursé !!
                    </p>
                    <p class="title-list">Liste des filleuls/es</p>
                    <ol class="list-godson">'.$list_html.'</ol>
                    <p class="title-list">Remboursement Fait / A Faire</p>
                    <ol class="refund-list">'.$list_refund_html.'</ol>
                    <div class="group-btn">
                        <button onclick="submitFormRefund('.$user_id.')">Rembourser</button>
                        <button onclick="closePopupUSerDetails()">Fermer</button>
                    </div>
                 </div>';

    echo $html;

    die();
}
add_action( 'wp_ajax_getPopupUserDetailsSuperYeti', 'getPopupUserDetailsSuperYeti' );
add_action( 'wp_ajax_nopriv_getPopupUserDetailsSuperYeti', 'getPopupUserDetailsSuperYeti' );

function getRefundUser() {

    $user_id = $_POST['user_id'];
    $json = stripslashes($_POST['json']);
    $elements = json_decode($json);

    foreach($elements as $element){
        $where = array();
        $where['id'] = $element;
        YetiPassRefundModel::updateYetiPassRefundAsRefund($where);
    }

    $results = SuperYetiUserModel::getSuperYetiCodeByUserId($user_id);
    $refund_results = YetiPassRefundModel::getYetiPassRefund($results[0]->id);

    $list_refund_html = '';
    foreach($refund_results as $result_refund) {
        $date_got = new DateTime($result_refund->got_date);
        if($result_refund->refund){
            $date_refund = new DateTime($result_refund->refund_date);
            $list_refund_html .= '<li><span class="dashicons dashicons-yes"></span><label>Obtenu le '.$date_got->format('d/m/Y').', remboursé le '.$date_refund->format('d/m/Y').'</label></li>';
        }else{
            $list_refund_html .= '<li><input type="checkbox" id="'.$result_refund->id.'" name="refund-check" value="1"><label>Obtenu le '.$date_got->format('d/m/Y').'</label></li>';
        }
    }

    echo $list_refund_html;

    die();

}
add_action( 'wp_ajax_getRefundUser', 'getRefundUser' );
add_action( 'wp_ajax_nopriv_getRefundUser', 'getRefundUser' );