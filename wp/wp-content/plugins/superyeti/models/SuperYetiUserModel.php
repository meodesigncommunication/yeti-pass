<?php

class SuperYetiUserModel
{
    private static $table = 'meo_super_yeti_user';

    /*
     * GET SUPER YETI BY USER ID IN TABLE meo_super_yeti_user
     */
    public static function getSuperYetiCodeByUserId($user_id){
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' ';
        $query .= 'WHERE user_id = '.$user_id;
        return $wpdb->get_results($query);
    }

    /*
     * GET SUPER YETI BY USER ID IN TABLE meo_super_yeti_user
     */
    public static function getSuperYetiCodeBySuperYetiCode($code){
        global $wpdb;
        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' ';
        $query .= 'WHERE code = "'.$code.'"';
        return $wpdb->get_results($query);
    }

    /*
     * INSERT DATA IN TABLE meo_super_yeti_user
     */
    public static function setSuperYetiUser($user_id,$super_yeti){
        global $wpdb;

        $data = array();
        $data['user_id'] = $user_id;
        $data['code'] = $super_yeti;
        $data['created_at'] = current_time( 'mysql' );
        $data['updated_at'] = current_time( 'mysql' );

        if($wpdb->insert(static::$table,$data)) {
            return true;
        }

        return false;
    }

}