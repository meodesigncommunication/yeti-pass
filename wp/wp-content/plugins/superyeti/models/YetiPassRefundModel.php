<?php

class YetiPassRefundModel
{
    private static $table = 'meo_yeti_pass_refund';

    public static function getYetiPassRefund($super_yeti_id){

        global $wpdb;

        $query  = 'SELECT * ';
        $query .= 'FROM '.static::$table.' ';
        $query .= 'WHERE super_yeti_id = '.$super_yeti_id.'';
        $results = $wpdb->get_results($query);

        return $results;
    }

    public static function setYetiPassRefund($super_yeti_id){
        global $wpdb;

        $data = array();
        $data['super_yeti_id'] = $super_yeti_id;
        $data['got_date'] = current_time( 'mysql' );

        if($wpdb->insert(static::$table,$data)) {
            return true;
        }

        return false;
    }

    public static function updateYetiPassRefundAsRefund($where){
        global $wpdb;

        $data = array();
        $data['refund_date'] = current_time( 'mysql' );
        $data['refund'] = 1;

        if($wpdb->update(static::$table,$data,$where)) {
            return true;
        }

        return false;
    }

}