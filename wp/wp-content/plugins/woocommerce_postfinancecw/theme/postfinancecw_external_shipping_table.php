
<div class="postfinancecw-external-checkout-shipping">
	<?php if (!empty($errorMessage)): ?>
		<p class="payment-error woocommerce-error">
			<?php print $errorMessage; ?>
		</p>
	<?php endif; ?>
	<h3><?php echo __("Shipping Option", "woocommerce_postfinancecw")?></h3>
	<table class="postfinancecw-external-checkout-shipping-table">
	
	<?php 
	echo $rows;
	?>
	
	</table>
	<input type="submit" class="postfinancecw-external-checkout-shipping-method-save-btn button btn btn-success postfinancecw-external-checkout-button" name="save" value="<?php echo __("Save Shipping Method", "woocommerce_postfinancecw"); ?>" data-loading-text="<?php echo __("Processing...", "woocommerce_postfinancecw"); ?>">
	
	
	<script type="text/javascript">
	jQuery(function(){
		jQuery('.postfinancecw-external-checkout-shipping-method-save-btn').hide();
	
		
		jQuery('.postfinancecw-external-checkout-shipping-table  input:radio').on('change', function(){
					jQuery('.postfinancecw-external-checkout-shipping-method-save-btn').click();
				
		});
	});
	</script>
</div>
