var postfinanceFlexCheckout =  new function () {
	
	this.includeCss = function(url){
		var cssId = 'postfinance-overlay';
		if (!document.getElementById(cssId))
		{
		    var head  = document.getElementsByTagName('head')[0];
		    var link  = document.createElement('link');
		    link.id   = cssId;
		    link.rel  = 'stylesheet';
		    link.type = 'text/css';
		    link.href = url;
		    link.media = 'all';
		    head.appendChild(link);
		}
	}
	
	this.createIframe = function(url, jQ){
		var over = 
			'<div id="postfinance-flex-overlay" class="postfinance-flex-overlay">'+
				
					'<iframe src="'+url+'" class="postfinance-flex-content"></iframe>'+
			'</div>';
		jQ(over).appendTo('body');
		jQ('body').addClass('postfinance-flex-noscroll');
	}

}