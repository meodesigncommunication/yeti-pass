<?php

require_once 'PostFinanceCw/Util.php';
require_once 'PostFinanceCw/BackendFormRenderer.php';
require_once 'Customweb/Util/Url.php';
require_once 'Customweb/Payment/Authorization/DefaultInvoiceItem.php';
require_once 'Customweb/Payment/BackendOperation/Adapter/Service/ICapture.php';
require_once 'Customweb/Form/Control/IEditableControl.php';
require_once 'Customweb/Payment/BackendOperation/Adapter/Service/ICancel.php';
require_once 'Customweb/IForm.php';
require_once 'Customweb/Form.php';
require_once 'Customweb/Core/Http/ContextRequest.php';
require_once 'Customweb/Form/Control/MultiControl.php';
require_once 'Customweb/Util/Currency.php';
require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
require_once 'Customweb/Payment/BackendOperation/Adapter/Service/IRefund.php';
require_once 'Customweb/Licensing/PostFinanceCw/License.php';



// Make sure we don't expose any info if called directly        	    	  		  	  
if (!function_exists('add_action')) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit();
}

// Add some CSS and JS for admin        	    	  		  	  
function woocommerce_postfinancecw_admin_add_setting_styles_scripts(){
	wp_register_style('woocommerce_postfinancecw_admin_styles', plugins_url('resources/css/settings.css', __FILE__));
	wp_enqueue_style('woocommerce_postfinancecw_admin_styles');
	
	wp_register_script('woocommerce_postfinancecw_admin_js', plugins_url('resources/js/settings.js', __FILE__));
	wp_enqueue_script('woocommerce_postfinancecw_admin_js');
}
add_action('admin_init', 'woocommerce_postfinancecw_admin_add_setting_styles_scripts');

function woocommerce_postfinancecw_admin_notice_handler(){
	if (get_transient(get_current_user_id() . '_postfinancecw_am') !== false) {
		
		foreach (get_transient(get_current_user_id() . '_postfinancecw_am') as $message) {
			$cssClass = '';
			if (strtolower($message['type']) == 'error') {
				$cssClass = 'error';
			}
			else if (strtolower($message['type']) == 'info') {
				$cssClass = 'updated';
			}
			
			echo '<div class="' . $cssClass . '">';
			echo '<p>PostFinance: ' . $message['message'] . '</p>';
			echo '</div>';
		}
		delete_transient(get_current_user_id() . '_postfinancecw_am');
	}
}
add_action('admin_notices', 'woocommerce_postfinancecw_admin_notice_handler');

function woocommerce_postfinancecw_admin_show_message($message, $type){
	$existing = array();
	if (get_transient(get_current_user_id() . '_postfinancecw_am') === false) {
		$existing = get_transient(get_current_user_id() . '_postfinancecw_am');
	}
	$existing[] = array(
		'message' => $message,
		'type' => $type 
	);
	set_transient(get_current_user_id() . '_postfinancecw_am', $existing);
}

/**
 * Add the configuration menu
 */
function woocommerce_postfinancecw_menu(){
	add_menu_page('PostFinance', __('PostFinance', 'woocommerce_postfinancecw'), 
			'manage_woocommerce', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw_options');
	
	if (isset($_REQUEST['page']) && strpos($_REQUEST['page'], 'woocommerce-postfinancecw') !== false) {
		$container = PostFinanceCw_Util::createContainer();
		if ($container->hasBean('Customweb_Payment_BackendOperation_Form_IAdapter')) {
			$adapter = $container->getBean('Customweb_Payment_BackendOperation_Form_IAdapter');
			foreach ($adapter->getForms() as $form) {
				add_submenu_page('woocommerce-postfinancecw', 'PostFinance ' . $form->getTitle(), $form->getTitle(), 
						'manage_woocommerce', 'woocommerce-postfinancecw' . $form->getMachineName(), 
						'woocommerce_postfinancecw_extended_options');
			}
		}
	}
	
	add_submenu_page(null, 'PostFinance Capture', 'PostFinance Capture', 'manage_woocommerce', 
			'woocommerce-postfinancecw_capture', 'woocommerce_postfinancecw_render_capture');
	add_submenu_page(null, 'PostFinance Cancel', 'PostFinance Cancel', 'manage_woocommerce', 
			'woocommerce-postfinancecw_cancel', 'woocommerce_postfinancecw_render_cancel');
	add_submenu_page(null, 'PostFinance Refund', 'PostFinance Refund', 'manage_woocommerce', 
			'woocommerce-postfinancecw_refund', 'woocommerce_postfinancecw_render_refund');
}
add_action('admin_menu', 'woocommerce_postfinancecw_menu');

function woocommerce_postfinancecw_render_cancel(){
	
	
	
	

	$request = Customweb_Core_Http_ContextRequest::getInstance();
	$query = $request->getParsedQuery();
	$post = $request->getParsedBody();
	$transactionId = $query['cwTransactionId'];
	
	if (empty($transactionId)) {
		wp_redirect(get_option('siteurl') . '/wp-admin');
		exit();
	}
	
	$transaction = PostFinanceCw_Util::getTransactionById($transactionId);
	$orderId = $transaction->getPostId();
	$url = str_replace('>orderId', $orderId, get_admin_url() . 'post.php?post=>orderId&action=edit');
	if ($request->getMethod() == 'POST') {
		if (isset($post['cancel'])) {
			$adapter = PostFinanceCw_Util::createContainer()->getBean('Customweb_Payment_BackendOperation_Adapter_Service_ICancel');
			if (!($adapter instanceof Customweb_Payment_BackendOperation_Adapter_Service_ICancel)) {
				throw new Exception("No adapter with interface 'Customweb_Payment_BackendOperation_Adapter_Service_ICancel' provided.");
			}
			
			try {
				$adapter->cancel($transaction->getTransactionObject());
				woocommerce_postfinancecw_admin_show_message(
						__("Successfully cancelled the transaction.", 'woocommerce_postfinancecw'), 'info');
			}
			catch (Exception $e) {
				woocommerce_postfinancecw_admin_show_message($e->getMessage(), 'error');
			}
			PostFinanceCw_Util::getEntityManager()->persist($transaction);
		}
		wp_redirect($url);
		exit();
	}
	else {
		if (!$transaction->getTransactionObject()->isCancelPossible()) {
			woocommerce_postfinancecw_admin_show_message(__('Cancel not possible', 'woocommerce_postfinancecw'), 'info');
			wp_redirect($url);
			exit();
		}
		if (isset($_GET['noheader'])) {
			require_once (ABSPATH . 'wp-admin/admin-header.php');
		}
		
		echo '<div class="wrap">';
		echo '<form method="POST" class="postfinancecw-line-item-grid" id="cancel-form">';
		echo '<table class="list">
				<tbody>';
		echo '<tr>
				<td class="left-align">' . __('Are you sure you want to cancel this transaction?', 'woocommerce_postfinancecw') . '</td>
			</tr>';
		echo '<tr>
				<td colspan="1" class="left-align"><a class="button" href="' . $url . '">' . __('No', 'woocommerce_postfinancecw') . '</a></td>
				<td colspan="1" class="right-align">
					<input class="button" type="submit" name="cancel" value="' . __('Yes', 'woocommerce_postfinancecw') . '" />
				</td>
			</tr>
								</tfoot>
			</table>
		</form>';
		
		echo '</div>';
	}
	
	
}

function woocommerce_postfinancecw_render_capture(){
	
	
	
	$request = Customweb_Core_Http_ContextRequest::getInstance();
	$query = $request->getParsedQuery();
	$post = $request->getParsedBody();
	$transactionId = $query['cwTransactionId'];
	
	if (empty($transactionId)) {
		wp_redirect(get_option('siteurl') . '/wp-admin');
		exit();
	}
	
	$transaction = PostFinanceCw_Util::getTransactionById($transactionId);
	$orderId = $transaction->getPostId();
	$url = str_replace('>orderId', $orderId, get_admin_url() . 'post.php?post=>orderId&action=edit');
	if ($request->getMethod() == 'POST') {
		
		if (isset($post['quantity'])) {
			
			$captureLineItems = array();
			$lineItems = $transaction->getTransactionObject()->getUncapturedLineItems();
			foreach ($post['quantity'] as $index => $quantity) {
				if (isset($post['price_including'][$index]) && floatval($post['price_including'][$index]) != 0) {
					$originalItem = $lineItems[$index];
					if ($originalItem->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
						$priceModifier = -1;
					}
					else {
						$priceModifier = 1;
					}
					$captureLineItems[$index] = new Customweb_Payment_Authorization_DefaultInvoiceItem($originalItem->getSku(), 
							$originalItem->getName(), $originalItem->getTaxRate(), $priceModifier * floatval($post['price_including'][$index]), 
							$quantity, $originalItem->getType());
				}
			}
			if (count($captureLineItems) > 0) {
				$adapter = PostFinanceCw_Util::createContainer()->getBean('Customweb_Payment_BackendOperation_Adapter_Service_ICapture');
				if (!($adapter instanceof Customweb_Payment_BackendOperation_Adapter_Service_ICapture)) {
					throw new Exception("No adapter with interface 'Customweb_Payment_BackendOperation_Adapter_Service_ICapture' provided.");
				}
				
				$close = false;
				if (isset($post['close']) && $post['close'] == 'on') {
					$close = true;
				}
				try {
					$adapter->partialCapture($transaction->getTransactionObject(), $captureLineItems, $close);
					woocommerce_postfinancecw_admin_show_message(
							__("Successfully added a new capture.", 'woocommerce_postfinancecw'), 'info');
				}
				catch (Exception $e) {
					woocommerce_postfinancecw_admin_show_message($e->getMessage(), 'error');
				}
				PostFinanceCw_Util::getEntityManager()->persist($transaction);
			}
		}
		
		wp_redirect($url);
		exit();
	}
	else {
		if (!$transaction->getTransactionObject()->isPartialCapturePossible()) {
			woocommerce_postfinancecw_admin_show_message(__('Capture not possible', 'woocommerce_postfinancecw'), 'info');
			
			wp_redirect($url);
			exit();
		}
		if (isset($_GET['noheader'])) {
			require_once (ABSPATH . 'wp-admin/admin-header.php');
		}
		
		echo '<div class="wrap">';
		echo '<form method="POST" class="postfinancecw-line-item-grid" id="capture-form">';
		echo '<input type="hidden" id="postfinancecw-decimal-places" value="' .
				 Customweb_Util_Currency::getDecimalPlaces($transaction->getTransactionObject()->getCurrencyCode()) . '" />';
		echo '<input type="hidden" id="postfinancecw-currency-code" value="' . strtoupper($transaction->getTransactionObject()->getCurrencyCode()) .
				 '" />';
		echo '<table class="list">
					<thead>
						<tr>
						<th class="left-align">' . __('Name', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('SKU', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('Type', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('Tax Rate', 'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Quantity', 
				'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Total Amount (excl. Tax)', 'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Total Amount (incl. Tax)', 'woocommerce_postfinancecw') . '</th>
						</tr>
				</thead>
				<tbody>';
		foreach ($transaction->getTransactionObject()->getUncapturedLineItems() as $index => $item) {
			
			$amountExcludingTax = Customweb_Util_Currency::formatAmount($item->getAmountExcludingTax(), 
					$transaction->getTransactionObject()->getCurrencyCode());
			$amountIncludingTax = Customweb_Util_Currency::formatAmount($item->getAmountIncludingTax(), 
					$transaction->getTransactionObject()->getCurrencyCode());
			if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
				$amountExcludingTax = $amountExcludingTax * -1;
				$amountIncludingTax = $amountIncludingTax * -1;
			}
			echo '<tr id="line-item-row-' . $index . '" class="line-item-row" data-line-item-index="' . $index, '" >
						<td class="left-align">' . $item->getName() . '</td>
						<td class="left-align">' . $item->getSku() . '</td>
						<td class="left-align">' . $item->getType() . '</td>
						<td class="left-align">' . round($item->getTaxRate(), 2) . ' %<input type="hidden" class="tax-rate" value="' . $item->getTaxRate() . '" /></td>
						<td class="right-align"><input type="text" class="line-item-quantity" name="quantity[' . $index . ']" value="' . $item->getQuantity() . '" /></td>
						<td class="right-align"><input type="text" class="line-item-price-excluding" name="price_excluding[' . $index . ']" value="' .
					 $amountExcludingTax . '" /></td>
						<td class="right-align"><input type="text" class="line-item-price-including" name="price_including[' . $index . ']" value="' .
					 $amountIncludingTax . '" /></td>
					</tr>';
		}
		echo '</tbody>
				<tfoot>
					<tr>
						<td colspan="6" class="right-align">' . __('Total Capture Amount', 'woocommerce_postfinancecw') . ':</td>
						<td id="line-item-total" class="right-align">' . Customweb_Util_Currency::formatAmount(
				$transaction->getTransactionObject()->getCapturableAmount(), $transaction->getTransactionObject()->getCurrencyCode()) .
				 strtoupper($transaction->getTransactionObject()->getCurrencyCode()) . '
					</tr>';
		
		if ($transaction->getTransactionObject()->isCaptureClosable()) {
			
			echo '<tr>
					<td colspan="7" class="right-align">
						<label for="close-transaction">' . __('Close transaction for further captures', 'woocommerce_postfinancecw') . '</label>
						<input id="close-transaction" type="checkbox" name="close" value="on" />
					</td>
				</tr>';
		}
		
		echo '<tr>
				<td colspan="2" class="left-align"><a class="button" href="' . $url . '">' . __('Back', 'woocommerce_postfinancecw') . '</a></td>
				<td colspan="5" class="right-align">
					<input class="button" type="submit" value="' . __('Capture', 'woocommerce_postfinancecw') . '" />
				</td>
			</tr>
			</tfoot>
			</table>
		</form>';
		
		echo '</div>';
	}
	
	
}

function woocommerce_postfinancecw_render_refund(){
	
	
	
	$request = Customweb_Core_Http_ContextRequest::getInstance();
	$query = $request->getParsedQuery();
	$post = $request->getParsedBody();
	$transactionId = $query['cwTransactionId'];
	
	if (empty($transactionId)) {
		wp_redirect(get_option('siteurl') . '/wp-admin');
		exit();
	}
	
	$transaction = PostFinanceCw_Util::getTransactionById($transactionId);
	$orderId = $transaction->getPostId();
	$url = str_replace('>orderId', $orderId, get_admin_url() . 'post.php?post=>orderId&action=edit');
	if ($request->getMethod() == 'POST') {
		
		if (isset($post['quantity'])) {
			
			$refundLineItems = array();
			$lineItems = $transaction->getTransactionObject()->getNonRefundedLineItems();
			foreach ($post['quantity'] as $index => $quantity) {
				if (isset($post['price_including'][$index]) && floatval($post['price_including'][$index]) != 0) {
					$originalItem = $lineItems[$index];
					if ($originalItem->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
						$priceModifier = -1;
					}
					else {
						$priceModifier = 1;
					}
					$refundLineItems[$index] = new Customweb_Payment_Authorization_DefaultInvoiceItem($originalItem->getSku(), 
							$originalItem->getName(), $originalItem->getTaxRate(), $priceModifier * floatval($post['price_including'][$index]), 
							$quantity, $originalItem->getType());
				}
			}
			if (count($refundLineItems) > 0) {
				$adapter = PostFinanceCw_Util::createContainer()->getBean('Customweb_Payment_BackendOperation_Adapter_Service_IRefund');
				if (!($adapter instanceof Customweb_Payment_BackendOperation_Adapter_Service_IRefund)) {
					throw new Exception("No adapter with interface 'Customweb_Payment_BackendOperation_Adapter_Service_IRefund' provided.");
				}
				
				$close = false;
				if (isset($post['close']) && $post['close'] == 'on') {
					$close = true;
				}
				try {
					$adapter->partialRefund($transaction->getTransactionObject(), $refundLineItems, $close);
					woocommerce_postfinancecw_admin_show_message(
							__("Successfully added a new refund.", 'woocommerce_postfinancecw'), 'info');
				}
				catch (Exception $e) {
					woocommerce_postfinancecw_admin_show_message($e->getMessage(), 'error');
				}
				PostFinanceCw_Util::getEntityManager()->persist($transaction);
			}
		}
		wp_redirect($url);
		exit();
	}
	else {
		if (!$transaction->getTransactionObject()->isPartialRefundPossible()) {
			woocommerce_postfinancecw_admin_show_message(__('Refund not possible', 'woocommerce_postfinancecw'), 'info');
			wp_redirect($url);
			exit();
		}
		if (isset($query['noheader'])) {
			require_once (ABSPATH . 'wp-admin/admin-header.php');
		}
		
		echo '<div class="wrap">';
		echo '<form method="POST" class="postfinancecw-line-item-grid" id="refund-form">';
		echo '<input type="hidden" id="postfinancecw-decimal-places" value="' .
				 Customweb_Util_Currency::getDecimalPlaces($transaction->getTransactionObject()->getCurrencyCode()) . '" />';
		echo '<input type="hidden" id="postfinancecw-currency-code" value="' . strtoupper($transaction->getTransactionObject()->getCurrencyCode()) .
				 '" />';
		echo '<table class="list">
					<thead>
						<tr>
						<th class="left-align">' . __('Name', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('SKU', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('Type', 'woocommerce_postfinancecw') . '</th>
						<th class="left-align">' . __('Tax Rate', 'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Quantity', 
				'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Total Amount (excl. Tax)', 'woocommerce_postfinancecw') . '</th>
						<th class="right-align">' . __('Total Amount (incl. Tax)', 'woocommerce_postfinancecw') . '</th>
						</tr>
				</thead>
				<tbody>';
		foreach ($transaction->getTransactionObject()->getNonRefundedLineItems() as $index => $item) {
			$amountExcludingTax = Customweb_Util_Currency::formatAmount($item->getAmountExcludingTax(), 
					$transaction->getTransactionObject()->getCurrencyCode());
			$amountIncludingTax = Customweb_Util_Currency::formatAmount($item->getAmountIncludingTax(), 
					$transaction->getTransactionObject()->getCurrencyCode());
			if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
				$amountExcludingTax = $amountExcludingTax * -1;
				$amountIncludingTax = $amountIncludingTax * -1;
			}
			echo '<tr id="line-item-row-' . $index . '" class="line-item-row" data-line-item-index="' . $index, '" >
					<td class="left-align">' . $item->getName() . '</td>
					<td class="left-align">' . $item->getSku() . '</td>
					<td class="left-align">' . $item->getType() . '</td>
					<td class="left-align">' . round($item->getTaxRate(), 2) . ' %<input type="hidden" class="tax-rate" value="' . $item->getTaxRate() . '" /></td>
					<td class="right-align"><input type="text" class="line-item-quantity" name="quantity[' . $index . ']" value="' . $item->getQuantity() . '" /></td>
					<td class="right-align"><input type="text" class="line-item-price-excluding" name="price_excluding[' . $index . ']" value="' .
					 $amountExcludingTax . '" /></td>
					<td class="right-align"><input type="text" class="line-item-price-including" name="price_including[' . $index . ']" value="' .
					 $amountIncludingTax . '" /></td>
				</tr>';
		}
		echo '</tbody>
				<tfoot>
					<tr>
						<td colspan="6" class="right-align">' . __('Total Refund Amount', 'woocommerce_postfinancecw') . ':</td>
						<td id="line-item-total" class="right-align">' . Customweb_Util_Currency::formatAmount(
				$transaction->getTransactionObject()->getRefundableAmount(), $transaction->getTransactionObject()->getCurrencyCode()) .
				 strtoupper($transaction->getTransactionObject()->getCurrencyCode()) . '
						</tr>';
		
		if ($transaction->getTransactionObject()->isRefundClosable()) {
			echo '<tr>
					<td colspan="7" class="right-align">
						<label for="close-transaction">' . __('Close transaction for further refunds', 'woocommerce_postfinancecw') . '</label>
						<input id="close-transaction" type="checkbox" name="close" value="on" />
					</td>
				</tr>';
		}
		
		echo '<tr>
				<td colspan="2" class="left-align"><a class="button" href="' . $url . '">' . __('Back', 'woocommerce_postfinancecw') . '</a></td>
				<td colspan="5" class="right-align">
					<input class="button" type="submit" value="' . __('Refund', 'woocommerce_postfinancecw') . '" />
				</td>
			</tr>
		</tfoot>
		</table>
		</form>';
		
		echo '</div>';
	}
	
	
}

function woocommerce_postfinancecw_extended_options(){
	$container = PostFinanceCw_Util::createContainer();
	$request = Customweb_Core_Http_ContextRequest::getInstance();
	$query = $request->getParsedQuery();
	$formName = substr($query['page'], strlen('woocommerce-postfinancecw'));
	
	$renderer = new PostFinanceCw_BackendFormRenderer();
	
	if ($container->hasBean('Customweb_Payment_BackendOperation_Form_IAdapter')) {
		$adapter = $container->getBean('Customweb_Payment_BackendOperation_Form_IAdapter');
		
		foreach ($adapter->getForms() as $form) {
			if ($form->getMachineName() == $formName) {
				$currentForm = $form;
				break;
			}
		}
		if ($currentForm === null) {
			if (isset($query['noheader'])) {
				require_once (ABSPATH . 'wp-admin/admin-header.php');
			}
			return;
		}
		
		if ($request->getMethod() == 'POST') {
			
			$pressedButton = null;
			$body = stripslashes_deep($request->getParsedBody());
			foreach ($form->getButtons() as $button) {
				
				if (array_key_exists($button->getMachineName(), $body['button'])) {
					$pressedButton = $button;
					break;
				}
			}
			$formData = array();
			foreach ($form->getElements() as $element) {
				$control = $element->getControl();
				if (!($control instanceof Customweb_Form_Control_IEditableControl)) {
					continue;
				}
				$dataValue = $control->getFormDataValue($body);
				if ($control instanceof Customweb_Form_Control_MultiControl) {
					foreach (woocommerce_postfinancecw_array_flatten($dataValue) as $key => $value) {
						$formData[$key] = $value;
					}
				}
				else {
					$nameAsArray = $control->getControlNameAsArray();
					if (count($nameAsArray) > 1) {
						$tmpArray = array(
							$nameAsArray[count($nameAsArray) - 1] => $dataValue 
						);
						$iterator = count($nameAsArray) - 2;
						while ($iterator > 0) {
							$tmpArray = array(
								$nameAsArray[$iterator] => $tmpArray 
							);
							$iterator--;
						}
						if (isset($formData[$nameAsArray[0]])) {
							$formData[$nameAsArray[0]] = array_merge_recursive($formData[$nameAsArray[0]], $tmpArray);
						}
						else {
							$formData[$nameAsArray[0]] = $tmpArray;
						}
					}
					else {
						$formData[$control->getControlName()] = $dataValue;
					}
				}
			}
			$adapter->processForm($currentForm, $pressedButton, $formData);
			wp_redirect(Customweb_Util_Url::appendParameters($request->getUrl(), $request->getParsedQuery()));
			die();
		}
		
		if (isset($query['noheader'])) {
			require_once (ABSPATH . 'wp-admin/admin-header.php');
		}
		
		$currentForm = null;
		foreach ($adapter->getForms() as $form) {
			if ($form->getMachineName() == $formName) {
				$currentForm = $form;
				break;
			}
		}
		
		if ($currentForm->isProcessable()) {
			$currentForm = new Customweb_Form($currentForm);
			$currentForm->setRequestMethod(Customweb_IForm::REQUEST_METHOD_POST);
			$currentForm->setTargetUrl(
					Customweb_Util_Url::appendParameters($request->getUrl(), 
							array_merge($request->getParsedQuery(), array(
								'noheader' => 'true' 
							))));
		}
		echo '<div class="wrap">';
		echo $renderer->renderForm($currentForm);
		echo '</div>';
	}
}

function woocommerce_postfinancecw_array_flatten($array){
	$return = array();
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			$return = array_merge($return, woocommerce_postfinancecw_array_flatten($value));
		}
		else {
			$return[$key] = $value;
		}
	}
	return $return;
}

/**
 * Setup the configuration page with the callbacks to the configuration API.
 */
function woocommerce_postfinancecw_options(){
	if (!current_user_can('manage_woocommerce')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}
	require_once 'Customweb/Licensing/PostFinanceCw/License.php';
Customweb_Licensing_PostFinanceCw_License::run('ho9s3bulnp2jt31r');
	echo '<div class="wrap">';
	
	echo '<form method="post" action="options.php" enctype="multipart/form-data">';
	settings_fields('woocommerce-postfinancecw');
	do_settings_sections('woocommerce-postfinancecw');
	
	echo '<p class="submit">';
	echo '<input type="submit" name="submit" id="submit" class="button-primary" value="' . __('Save Changes') . '" />';
	echo '</p>';
	
	echo '</form>';
	echo '</div>';
}



/**
 * Register Settings
 */
function woocommerce_postfinancecw_admin_init(){
	add_settings_section('woocommerce_postfinancecw', 'PostFinance Basics', 
			'woocommerce_postfinancecw_section_callback', 'woocommerce-postfinancecw');
	add_settings_field('woocommerce_postfinancecw_operation_mode', __("Operation Mode", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_operation_mode', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_operation_mode');
	
	add_settings_field('woocommerce_postfinancecw_pspid', __("Live PSPID", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_pspid', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_pspid');
	
	add_settings_field('woocommerce_postfinancecw_test_pspid', __("Test PSPID", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_test_pspid', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_test_pspid');
	
	add_settings_field('woocommerce_postfinancecw_live_sha_passphrase_in', __("SHA-IN Passphrase", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_live_sha_passphrase_in', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_live_sha_passphrase_in');
	
	add_settings_field('woocommerce_postfinancecw_live_sha_passphrase_out', __("SHA-OUT Passphrase", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_live_sha_passphrase_out', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_live_sha_passphrase_out');
	
	add_settings_field('woocommerce_postfinancecw_test_sha_passphrase_in', __("Test Account SHA-IN Passphrase", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_test_sha_passphrase_in', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_test_sha_passphrase_in');
	
	add_settings_field('woocommerce_postfinancecw_test_sha_passphrase_out', __("Test Account SHA-OUT Passphrase", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_test_sha_passphrase_out', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_test_sha_passphrase_out');
	
	add_settings_field('woocommerce_postfinancecw_hash_method', __("Hash calculation method", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_hash_method', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_hash_method');
	
	add_settings_field('woocommerce_postfinancecw_order_id_schema', __("Order prefix", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_order_id_schema', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_order_id_schema');
	
	add_settings_field('woocommerce_postfinancecw_title', __("Payment Page Title", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_title', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_title');
	
	add_settings_field('woocommerce_postfinancecw_order_description_schema', __("Order Description", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_order_description_schema', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_order_description_schema');
	
	add_settings_field('woocommerce_postfinancecw_template', __("Dynamic Template", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_template', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_template');
	
	add_settings_field('woocommerce_postfinancecw_template_url', __("Template URL for own template", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_template_url', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_template_url');
	
	add_settings_field('woocommerce_postfinancecw_shop_id', __("Shop ID", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_shop_id', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_shop_id');
	
	add_settings_field('woocommerce_postfinancecw_api_user_id', __("API Username", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_api_user_id', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_api_user_id');
	
	add_settings_field('woocommerce_postfinancecw_api_password', __("API Password", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_api_password', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_api_password');
	
	add_settings_field('woocommerce_postfinancecw_alias_usage_message', __("Intended purpose of alias", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_alias_usage_message', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_alias_usage_message');
	
	add_settings_field('woocommerce_postfinancecw_transaction_updates', __("Transaction Updates", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_transaction_updates', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_transaction_updates');
	
	add_settings_field('woocommerce_postfinancecw_review_input_form', __("Review Input Form", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_review_input_form', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_review_input_form');
	
	add_settings_field('woocommerce_postfinancecw_order_identifier', __("Order Identifier", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_order_identifier', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_order_identifier');
	
	add_settings_field('woocommerce_postfinancecw_external_checkout_placement', __("External Checkout: Widget Placement", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_external_checkout_placement', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_external_checkout_placement');
	
	add_settings_field('woocommerce_postfinancecw_external_checkout_account_creation', __("External Checkout: Guest Checkout", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_external_checkout_account_creation', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_external_checkout_account_creation');
	
	add_settings_field('woocommerce_postfinancecw_log_level', __("Log Level", 'woocommerce_postfinancecw'), 'woocommerce_postfinancecw_option_callback_log_level', 'woocommerce-postfinancecw', 'woocommerce_postfinancecw');
	register_setting('woocommerce-postfinancecw', 'woocommerce_postfinancecw_log_level');
	
	
}
add_action('admin_init', 'woocommerce_postfinancecw_admin_init');

function woocommerce_postfinancecw_section_callback(){}



function woocommerce_postfinancecw_option_callback_operation_mode() {
	echo '<select name="woocommerce_postfinancecw_operation_mode">';
		echo '<option value="test"';
		 if (get_option('woocommerce_postfinancecw_operation_mode', "test") == "test"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Test Mode", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="live"';
		 if (get_option('woocommerce_postfinancecw_operation_mode', "test") == "live"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Live Mode", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("If the test mode is selected the test PSPID is used and the test SHA passphrases.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_pspid() {
	echo '<input type="text" name="woocommerce_postfinancecw_pspid" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_pspid', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("The PSPID as given by the PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_test_pspid() {
	echo '<input type="text" name="woocommerce_postfinancecw_test_pspid" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_test_pspid', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("The test PSPID as given by the PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_live_sha_passphrase_in() {
	echo '<input type="text" name="woocommerce_postfinancecw_live_sha_passphrase_in" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_live_sha_passphrase_in', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Enter the live SHA-IN passphrase. This value must be identical to the one in the back-end of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_live_sha_passphrase_out() {
	echo '<input type="text" name="woocommerce_postfinancecw_live_sha_passphrase_out" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_live_sha_passphrase_out', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Enter the live SHA-OUT passphrase. This value must be identical to the one in the back-end of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_test_sha_passphrase_in() {
	echo '<input type="text" name="woocommerce_postfinancecw_test_sha_passphrase_in" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_test_sha_passphrase_in', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Enter the test SHA-IN passphrase. This value must be identical to the one in the back-end of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_test_sha_passphrase_out() {
	echo '<input type="text" name="woocommerce_postfinancecw_test_sha_passphrase_out" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_test_sha_passphrase_out', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Enter the test SHA-OUT passphrase. This value must be identical to the one in the back-end of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_hash_method() {
	echo '<select name="woocommerce_postfinancecw_hash_method">';
		echo '<option value="sha1"';
		 if (get_option('woocommerce_postfinancecw_hash_method', "sha512") == "sha1"){
			echo ' selected="selected" ';
		}
	echo '>' . __("SHA-1", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="sha256"';
		 if (get_option('woocommerce_postfinancecw_hash_method', "sha512") == "sha256"){
			echo ' selected="selected" ';
		}
	echo '>' . __("SHA-256", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="sha512"';
		 if (get_option('woocommerce_postfinancecw_hash_method', "sha512") == "sha512"){
			echo ' selected="selected" ';
		}
	echo '>' . __("SHA-512", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("Select the hash calculation method to use. This value must correspond with the selected value in the back-end of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_order_id_schema() {
	echo '<input type="text" name="woocommerce_postfinancecw_order_id_schema" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_order_id_schema', 'order_{id}'),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Here you can insert an order prefix. The prefix allows you to change the order number that is transmitted to PostFinance. The prefix must contain the tag {id}. It will then be replaced by the order number (e.g. name_{id}).", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_title() {
	echo '<textarea name="woocommerce_postfinancecw_title">' . get_option('woocommerce_postfinancecw_title', '') . '</textarea>';
	
	echo '<br />';
	echo __("Define here the title which is shown on the payment page. If no title is defined here the default one is used.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_order_description_schema() {
	echo '<input type="text" name="woocommerce_postfinancecw_order_description_schema" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_order_description_schema', 'Order {id}'),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("This parameter is sometimes transmitted to the acquirer (depending on the acquirer), in order to be shown on the account statements of the merchant or the customer. The prefix can contain the tag {id}. It will then be replaced by the order number (e.g. name {id}). (Payment Page only)", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_template() {
	echo '<select name="woocommerce_postfinancecw_template">';
		echo '<option value="default"';
		 if (get_option('woocommerce_postfinancecw_template', "default") == "default"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Use shop template", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="static"';
		 if (get_option('woocommerce_postfinancecw_template', "default") == "static"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Use static template", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="custom"';
		 if (get_option('woocommerce_postfinancecw_template', "default") == "custom"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Use own template", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="none"';
		 if (get_option('woocommerce_postfinancecw_template', "default") == "none"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Don't change the layout of the payment page", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("With the Dynamic Template you can design the layout of the payment page yourself. For the option 'Own template' the URL to the template file must be entered into the following box.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_template_url() {
	echo '<input type="text" name="woocommerce_postfinancecw_template_url" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_template_url', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("The URL indicated here is rendered as Template. For this you must select option 'Use own template'. The URL must point to an HTML page that contains the string '\$\$\$PAYMENT ZONE\$\$\$'. This part of the HTML file is replaced with the form for the credit card input.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_shop_id() {
	echo '<input type="text" name="woocommerce_postfinancecw_shop_id" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_shop_id', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Here you can define a Shop ID. This is only necessary if you wish to operate several shops with one PSPID. In order to use this module, an additional module is required.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_api_user_id() {
	echo '<input type="text" name="woocommerce_postfinancecw_api_user_id" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_api_user_id', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("You can create an API username in the back-end of PostFinance. The API user is necessary for the direct communication between the shop and the service of PostFinance.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_api_password() {
	echo '<input type="text" name="woocommerce_postfinancecw_api_password" value="' . htmlspecialchars(get_option('woocommerce_postfinancecw_api_password', ''),ENT_QUOTES) . '" />';
	
	echo '<br />';
	echo __("Password for the API user.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_alias_usage_message() {
	echo '<textarea name="woocommerce_postfinancecw_alias_usage_message">' . get_option('woocommerce_postfinancecw_alias_usage_message', '') . '</textarea>';
	
	echo '<br />';
	echo __("If the Alias Manager is used, the intended purpose is shown to the customer on the payment page. Through this the customer knows why his data is saved.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_transaction_updates() {
	echo '<select name="woocommerce_postfinancecw_transaction_updates">';
		echo '<option value="active"';
		 if (get_option('woocommerce_postfinancecw_transaction_updates', "inactive") == "active"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Active", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="inactive"';
		 if (get_option('woocommerce_postfinancecw_transaction_updates', "inactive") == "inactive"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Inactive", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("When the store is not available (network outage, server failure or any other outage), when the feedback of PostFinance is sent, then the transaction state is not updated. Hence no order confirmation e-mail is sent and the order is not in the paid state. By activating the transaction update, such transactions can be authorized later over direct link. To use this feature the update service must be activated and the API username and the API password must be set.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_review_input_form() {
	echo '<select name="woocommerce_postfinancecw_review_input_form">';
		echo '<option value="active"';
		 if (get_option('woocommerce_postfinancecw_review_input_form', "active") == "active"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Activate input form in review pane.", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="deactivate"';
		 if (get_option('woocommerce_postfinancecw_review_input_form', "active") == "deactivate"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Deactivate input form in review pane.", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("Should the input form for credit card data rendered in the review pane? To work the user must have JavaScript activated. In case the browser does not support JavaScript a fallback is provided. This feature is not supported by all payment methods.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_order_identifier() {
	echo '<select name="woocommerce_postfinancecw_order_identifier">';
		echo '<option value="postid"';
		 if (get_option('woocommerce_postfinancecw_order_identifier', "ordernumber") == "postid"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Post ID of the order", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="ordernumber"';
		 if (get_option('woocommerce_postfinancecw_order_identifier', "ordernumber") == "ordernumber"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Order number", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("Set which identifier should be sent to the payment service provider. If a plugin modifies the order number and can not guarantee it's uniqueness, select Post Id.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_external_checkout_placement() {
	echo '<select name="woocommerce_postfinancecw_external_checkout_placement">';
		echo '<option value="both"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_placement', "both") == "both"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Cart and Checkout page", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="cart"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_placement', "both") == "cart"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Cart Page only", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="checkout"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_placement', "both") == "checkout"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Checkout Page only", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="custom"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_placement', "both") == "custom"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Custom Action", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("Should the external checkout widgets be displayed on the cart page, checkout page, both, or placed with a custom action. If you use the Custom Action, you can display the widgets with through executing the action 'woocommerce_customweb_checkout_widgets' in your theme.", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_external_checkout_account_creation() {
	echo '<select name="woocommerce_postfinancecw_external_checkout_account_creation">';
		echo '<option value="force_selection"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_account_creation', "skip_selection") == "force_selection"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Force Account Selection", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="skip_selection"';
		 if (get_option('woocommerce_postfinancecw_external_checkout_account_creation', "skip_selection") == "skip_selection"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Create Guest Account when possible", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("When an external checkout is active the customer may need to authenticate. If the e-mail address does not exist in the database, should the customer be forced to select how he or she should create the account or should automatically an guest account be created?", 'woocommerce_postfinancecw');
}

function woocommerce_postfinancecw_option_callback_log_level() {
	echo '<select name="woocommerce_postfinancecw_log_level">';
		echo '<option value="error"';
		 if (get_option('woocommerce_postfinancecw_log_level', "error") == "error"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Error", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="info"';
		 if (get_option('woocommerce_postfinancecw_log_level', "error") == "info"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Info", 'woocommerce_postfinancecw'). '</option>';
	echo '<option value="debug"';
		 if (get_option('woocommerce_postfinancecw_log_level', "error") == "debug"){
			echo ' selected="selected" ';
		}
	echo '>' . __("Debug", 'woocommerce_postfinancecw'). '</option>';
	echo '</select>';
	echo '<br />';
	echo __("Messages of this or a higher level will be logged.", 'woocommerce_postfinancecw');
}

