<?php
    /*
     * Template name: One Pager
     */
    session_start();

    use Facebook\FacebookSession;
    use Facebook\FacebookRequest;
    use Facebook\GraphObject;
    use Facebook\GraphUser;
    use Facebook\FacebookRequestException;
    use \Facebook\FacebookRedirectLoginHelper;

    require_once (get_template_directory() . '/vendor/autoload.php');

    global $post;

    /* IMPORT DATA SLIDER */
    $slider = get_field('slider', $post->ID);
    $slider_title = get_field('slider_title', $post->ID);
    $slider_content = get_field('slider_content', $post->ID);
    $slider_button = get_field('slider_button', $post->ID);

    /* IMPORT DATA ACTIVITIES */
    $activities = get_field('activities',$post->ID);

    /* IMPORT DATA MAP */
    $text_map = get_field('text_map',$post->ID, false);
    $image_map = get_field('image_map',$post->ID);

    $text_map_2 = get_field('text_map_2',$post->ID, false);
    $image_map_2 = get_field('image_map_2',$post->ID);

    /* IMPORT DATA SUPERYETI SETTING */
    $super_yeti = get_field('super_yeti_data',$post->ID);

    /* IMPORT DATA EVENTS */
    $events = query_posts('post_type=events');

    /* TEST FACEBOOK */

    $appId = '1157089911102079';
    $appSecret = '1811eea0fdad8034a9ede3b9ca7d75ee';
    $redirect_url = 'http://yetipass.meomeo.ch/';

    FacebookSession::setDefaultApplication($appId,$appSecret);
    $helper = new FacebookRedirectLoginHelper($redirect_url);

?>
<?php get_header(); ?>
<div class="content" role="main">
    <div class="wrapper">

        <!-- SLIDER -->
        <div id="slider">
            <div class="triangle"></div>
            <img class="slider-deco" src="<?php bloginfo('template_directory'); ?>/images/Yeti_Slider.png" alt="Le yeti" />
            <?php foreach($slider as $key => $slide): ?>
                <article id="slide-<?php echo $key ?>" class="slide">
                    <div alt="<?php echo $slide['slide_title'] ?>" class="slide-image" style="background-image: url('<?php echo $slide['image'] ?>'); background-position: <?php echo $slide['position_v'] ?> <?php echo $slide['position_h'] ?>;">
                        <div class="slide-bg-gradient"></div>
                    </div>
                </article>
            <?php endforeach; ?>
            <div class="slider-content">
                <span id="txt-info-saison">Hiver 2017 - 2018</span>
                <h1><?php echo $slider_title ?></h1>
                <p><?php echo $slider_content ?></p>
                <a onclick="" target="_blank" class="btn-download" href="<?php echo $slider_button ?>" title="Commander votre YetiPass">
                    <?php echo __('[:fr]commander maintenant[:]') ?>
                </a>
            </div>
        </div>
        <!-- / SLIDER -->

        <!--<div id="countdown"></div>-->

        <div class="first-txt-home">
            <span class="capital-txt"><?php echo $post->post_title ?></span>
            <?php echo do_shortcode($post->post_content) ?>
        </div>

        <!-- ACTIVITIES -->
        <?php if( !empty($activities) ): ?>
        <p id="activities" class="hometxt"><?php echo __('[:fr]<strong>La Braye</strong> c\'est:[:]') ?></p>
            <section class="activities">
                <?php foreach( $activities as $activity ):  ?>
                    <article style="background-image: url('<?php echo $activity['activity_image'] ?>');"><p><span><?php echo $activity['activity_texte'] ?></span></p></article>
                <?php endforeach; ?>
                <article>
                    <a href="<?php echo $slider_button ?>" title="Commander votre YetiPass">
                        <span>
                            Cette année, profitez <br/>de la neige pour le prix<br/>incroyable de 99.-
                        </span>
                        <span>
                            <img src="<?php bloginfo('template_directory'); ?>/images/5796_YetiPass_Mascotte1-4.png" alt="Le yeti veut un YETIPASS" />
                        </span>
                        <span>
                            Je commande
                        </span>
                    </a>
                </article>
            </section>
        <?php endif; ?>
        <!-- / ACTIVITIES -->

        <!-- PLAN LA BRAYE -->
            <p id="map" class="hometxt"><?php echo $text_map ?></p>
            <a href="<?php echo $image_map ?>" title="Agrandir la carte de la Braye">
                <img src="<?php echo $image_map ?>" alt="Carte de la Braye" />
            </a>
        <!-- / PLAN LA BRAYE -->

        <!-- PLAN LA CHATEAU D'OEX -->
        <p id="map" class="hometxt"><?php echo $text_map_2 ?></p>
        <a href="<?php echo $image_map_2 ?>" title="Agrandir la carte de Château d'Oex">
            <img src="<?php echo $image_map_2 ?>" alt="Carte de Château d'Oex" />
        </a>
        <!-- / PLAN LA CHATEAU D'OEX -->

        <!-- EVENT -->
        <p id="events" class="hometxt"><?php //echo __('[:fr]Ne manquez pas les Yetidays ![:]') ?></p>
        <section class="events">
            <?php if(!empty($events)): ?>
                <?php foreach($events as $event): ?>
                    <?php if(!empty($event->post_title)): ?>
                        <article>
                            <span class="date"><?php echo get_field('event_date',$event->ID) ?></span>
                            <h2><?php echo $event->post_title ?></h2>
                            <?php echo $event->post_content ?>
                        </article>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </section>
        <!-- / EVENT -->

        <!-- SUPER YETI -->
        <?php if($super_yeti): ?>
            <section class="super-yeti">
                <div><img src="<?php bloginfo('template_directory'); ?>/images/Yeti_superyeti.png" alt="Le Super Yeti vous remboursse votre abo" /></div>
                <div>
                    <p><?php echo __('[:fr]Devenez super-yeti !<br/>invitez vos amis[:]') ?></p>
                    <p><?php echo __('[:fr]A l’achat d’un 10ème Yetipass par vos amis<br/><strong>votre abonnement vous est remboursé*</strong>[:]') ?></p>
                    <a href="http://yetipass.ch/conditions-generales/" title="" class="button">
                        <?php echo __('[:fr]ça m\'intéresse[:]') ?>
                    </a>
                    <p class="small-txt">
                        <?php echo __('[:fr]*Vos amis qui utiliseront votre code super-yeti pour leurs achats<br/>recevront un cadeau spécial du yeti | <a href="'.home_url('/').'/conditions-generales" title="">voir la liste des cadeaux et des conditions</a>[:]') ?>
                    </p>
                </div>
            </section>
        <?php endif; ?>
        <!-- / SUPER YETI -->

        <!-- SOCIAL NETWORK -->
        <section class="social-network">
            <p class="hometxt"><?php echo __('[:fr]<strong>Suivez</strong> nous[:]') ?></p>
            <div id="fb-homepage-1">
                <p class="hometxt"><?php echo __('[:fr]Facebook[:]') ?></p>
                <div id="fb-homepage" class="fb-page" data-href="https://www.facebook.com/stationlabraye/" data-width="500" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/stationlabraye/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/stationlabraye/">Station été hiver Château-d&#039;Oex La Braye</a>
                    </blockquote>
                </div>
            </div>

            <div id="fb-homepage-2" class="fb-page" data-href="https://www.facebook.com/stationlabraye/" data-width="500" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <p class="hometxt"><?php echo __('[:fr]Instagram[:]') ?></p>
                <!-- LightWidget WIDGET -->
                <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                <iframe src="//lightwidget.com/widgets/cc31dc7db20453d78a960ca200695af6.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
            </div>

        </section>
        <!-- / SOCIAL NETWORK -->

        <!-- PARTENAIRES -->
        <section class="partners">
            <p class="hometxt"><?php echo __('[:fr]Liens <strong>utiles</strong>[:]') ?></p>
            <ul>
                <li><a target="_blank" href="https://www.chateau-doex.ch/fr/Z7181/tele-chateau-d-oex" title="Télé-Château-d’Oex">Télé-Château-d’Oex</a></li>
                <li><a target="_blank" href="https://www.chateau-doex.ch" title="Pays-d’Enhaut Tourisme">Pays-d’Enhaut Tourisme</a></li>
                <li><a target="_blank" href="https://www.chateau-doex.ch/Z4070" title="Ecole suisse de ski">Ecole suisse de ski</a></li>
                <li><a target="_blank" href="http://www.mob.ch/fr" title="Compagnie de chemin de fer Montreux Oberland Bernois">Compagnie de chemin de fer Montreux Oberland Bernois</a></li>
                <li><a target="_blank" href="https://www.chateau-doex.ch/fr/Z1841/patin-a-glace" title="La Patinoire à ciel ouvert de Château-d’Oex">La Patinoire à ciel ouvert de Château-d’Oex</a></li>
                <li><a target="_blank" href="http://www.musee-chateau-doex.ch" title="Musée du Vieux Pays-d’Enhaut">Musée du Vieux Pays-d’Enhaut</a></li>
                <li><a target="_blank" href="https://www.espace-ballon.ch" title="Espace Ballon">Espace Ballon</a></li>
            </ul>
        </section>
        <!-- / PARTENAIRES -->

        <?php

        /*if(isset($_SESSION) && isset($_SESSION['fb_token'])){
            $session = new FacebookSession($_SESSION['fb_token']);
        }else{
            $session = $helper->getSessionFromRedirect();
        }

        if ( isset( $session ) ) {
            try {
                // with this session I will post a message to my own timeline
                $request = new FacebookRequest(
                    $session,
                    'POST',
                    '/me/feed',
                    array(
                        'link' => 'www.finalwebsites.com/facebook-api-php-tutorial/',
                        'message' => 'A step by step tutorial on how to use Facebook PHP SDK v4.0'
                    )
                );
                $response = $request->execute();
            } catch ( FacebookRequestException $e ) {
                // show any error for this facebook request
                echo $e->getMessage();
            }
        }else{
            echo '<a href="'.$helper->getLoginUrl().'">CONNECT TO FACEBOOK</a>';
        }

        $link_test = 'http://www.facebook.com/dialog/feed?app_id='.$appId.'&link=http://developers.facebook.com/docs/reference/dialogs/&picture=http://fbrell.com/f8.jpg&name=Facebook%20Dialogs&caption=Reference%20Documentation&description=Dialogs%20provide%20a%20simple,%20consistent%20interface%20for%20applications%20to%20interact%20with%20users.&message=Facebook%20Dialogs%20are%20so%20easy!&redirect_uri=http://www.example.com/response';

        //echo '<a target="_blank" href="'.$link_test.'">CONNECT TO FACEBOOK</a>';*/

        ?>

        <!-- / SOCIAL NETWORK -->
        <span id="contact"></span>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/slider.js"></script>
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("Dec 16, 2017 09:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        var txt = 'La Braye ouvre ses portes dans<br/>';

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        if(days <= 1) {
            days = days + ' jour ';
        }else{
            days = days + ' jours ';
        }

        if(hours < 10) {
            hours = '0' + hours;
        }else{
            hours = hours;
        }

        if(minutes < 10) {
            minutes = '0' + minutes;
        }else{
            minutes = minutes;
        }

        if(seconds < 10) {
            seconds = '0' + seconds;
        }else{
            seconds = seconds;
        }

        // Display the result in the element with id="demo"
        document.getElementById("countdown").innerHTML = txt + '<span>' + days + hours + ":"
            + minutes + ":" + seconds + '</span>' ;

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "";
        }
    }, 1000);
</script>
<?php get_footer(); ?>