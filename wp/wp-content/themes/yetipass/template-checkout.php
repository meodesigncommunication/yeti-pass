<?php
/*
 * Template name: Checkout
 */
require_once 'includes/Mobile_Detect.php';

global $woocommerce, $post;

$detect = new Mobile_Detect();

$order_received = (!empty($_GET['key'])) ? true : false;
$request_url = $_SERVER["REQUEST_URI"];

$tab_url = explode('/',$request_url);
$index = count($tab_url)-2;
$order_id = $tab_url[$index];

$order = new WC_Order($order_id);
$payement_method = $order->payment_method;

if($payement_method == 'bacs') {

    $bacs = new WC_Gateway_BACS();

    $bacs_account_details = $bacs->account_details;
    $account_name = explode(',',$bacs_account_details[0]['account_name']);

}

do_action('start_checkout_order_receive');

?>
<?php get_header(); ?>
<div class="content" role="main" style="margin-bottom: 200px">
    <div class="wrapper">
        <div class="step">
            <ul>
                <li>
                        <span class="number">
                            <p>1</p>
                        </span>
                    <span>panier</span>
                    <?php if(!$order_received): ?>
                        <a href="<?php echo home_url( '/' ) ?>/cart"></a>
                    <?php endif; ?>
                </li>
                <?php if(!$order_received): ?>
                    <li class="active">
                <?php else: ?>
                    <li>
                <?php endif; ?>
                        <span class="number">
                            <p>2</p>
                        </span>
                    <span>compte et paiement</span>
                </li>
                <?php if($order_received): ?>
                    <li class="active">
                <?php else: ?>
                    <li>
                <?php endif; ?>
                    <span class="number">
                        <p>3</p>
                    </span>
                    <span>confirmation</span>
                </li>
            </ul>
        </div>
        <div class="bloc-checkout">
            <?php if($order_received): ?>
                <h2 class="thankyou-notice"><?php echo __('[:fr]merci pour votre commande ![:]') ?></h2>
                <?php if($payement_method == 'bacs'): ?>
                    <p class="picture-notice under-notice"><?php echo __('[:fr]Vous serez bientôt le détenteur d\'un Yetipass. Nous nous réjouissons de vous accueillir à la Braye. Votre commande sera validée par votre aimable réglement à l\'aide des détails ci-dessous.[:]') ?></p>
                    <h3><?php echo __('[:fr]Information pour virement bancaire[:]') ?></h3>
                    <?php if(!$detect->isMobile()): ?>
                        <table class="bank_details">
                            <tr>
                                <th><?php echo __('[:fr]En faveur de[:]') ?></th>
                                <th><?php echo __('[:fr]Nom de la banque[:]') ?></th>
                                <th><?php echo __('[:fr]N°compte ccp ('.$bacs_account_details[0]['bank_name'].')[:]') ?></th>
                                <th><?php echo __('[:fr]Iban[:]') ?></th>
                                <th><?php echo __('[:fr]Bic / swift[:]') ?></th>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        <?php foreach ($account_name as $data): ?>
                                            <?php echo $data . '<br/>' ?>
                                        <?php endforeach; ?>
                                    </p>
                                </td>
                                <td><?php echo $bacs_account_details[0]['bank_name'] ?></td>
                                <td><?php echo $bacs_account_details[0]['account_number'] ?></td>
                                <td><?php echo $bacs_account_details[0]['iban'] ?></td>
                                <td><?php echo $bacs_account_details[0]['bic'] ?></td>
                            </tr>
                        </table>
                    <?php else: ?>
                        <p class="picture-notice under-notice"><?php echo __('[:fr]Vous serez bientôt le détenteur d\'un Yetipass. Nous nous réjouissons de vous accueillir à la Braye. Votre commande sera validée par votre aimable réglement à l\'aide des détails ci-dessous.[:]') ?></p>
                        <table class="bank_details">
                            <tr>
                                <th><?php echo __('[:fr]En faveur de[:]') ?></th>
                                <td>
                                    <p>
                                        <?php foreach ($account_name as $data): ?>
                                            <?php echo $data . '<br/>' ?>
                                        <?php endforeach; ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo __('[:fr]Nom de la banque[:]') ?></th>
                                <td><?php echo $bacs_account_details[0]['bank_name'] ?></td>
                            </tr>
                            <tr>
                                <th><?php echo __('[:fr]N°compte ccp ('.$bacs_account_details[0]['bank_name'].')[:]') ?></th>
                                <td><?php echo $bacs_account_details[0]['account_number'] ?></td>
                            </tr>
                            <tr>
                                <th><?php echo __('[:fr]Iban[:]') ?></th>
                                <td><?php echo $bacs_account_details[0]['iban'] ?></td>
                            </tr>
                            <tr>
                                <th><?php echo __('[:fr]Bic / swift[:]') ?></th>
                                <td><?php echo $bacs_account_details[0]['bic'] ?></td>
                            </tr>
                        </table>
                    <?php endif; ?>
                <?php elseif($payement_method == 'PostFinanceCw_PostFinanceCard' || $payement_method == 'PostFinanceCw_PostFinanceEFinance' || $payement_method == 'PostFinanceCw_Visa' || $payement_method == 'PostFinanceCw_MasterCard' ): ?>
                    <p class="picture-notice under-notice"><?php echo __('[:fr]Vous serez bientôt le détenteur d\'un Yetipass. Nous nous réjouissons de vous accueillir à la Braye. Votre commande est validée par votre aimable paiement par carte.[:]') ?></p>
                <?php endif; ?>

                <h2 class="thankyou-notice notice-shipping"><?php echo __('[:fr]retirez votre yetipass dès le 4 décembre 2017<br/>au guichet des remontées mécaniques de La Braye[:]') ?></h2>
                <p class="picture-notice"><?php echo __('[:fr]Emporter la photo portrait qui figurera sur votre yetipass.<br/>Un système de prise de vue est également disponible au guichet.[:]') ?></p>
                <p class="adresse-notice">TELE-CHÂTEAU-D'OEX SA | Route de la Ray | 1660 Château-d'Oex</p>
            <?php endif; ?>
            <?php echo do_shortcode('[woocommerce_checkout]');  ?>
            <?php if($order_received): ?>
                <div class="group_btn">
                    <a href="<?php echo home_url( '/' ) ?>" title="<?php echo __('[:fr]Page d\'accueil[:]') ?>"><input type="button" name="back_home" value="<?php echo __('[:fr]revenir au site[:]') ?>" /></a>
                    <?php do_action( 'woocommerce_checkout_popup_yeti_open' ); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if($order_received): ?>
    <?php do_action( 'woocommerce_checkout_popup_yeti' ); ?>
<?php endif; ?>
<?php get_footer(); ?>