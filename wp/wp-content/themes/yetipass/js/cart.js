$(document).ready(function(){

    // Check the Yetipass quantity the minimum is 1
    var quantity = $('table#product-table-0').find('input').val()*1;
    var product_price = $('table#product-table-0').attr('data-price')*1;
    if(quantity < 1){
        $('table#product-table-0').find('input').val(1);
        $('table#product-table-0').find('.amount').html(product_price);
        cloneFormProduct($('table#product-table-0').attr('data-id'),1);
    }
    changeTotalCart();

});

/* ADD PRODUCT QUANTITY */
$('table.product button.add').click(function(e){
    e.preventDefault();
    var id = $(this).parents('table.product').attr('data-id');
    var parent_id = $(this).parents('table.product').attr('id');
    var input_value = $(this).parent().find('input').val()*1;
    $(this).parent().find('input').val(input_value+1);
    changeTotalPriceProduct(parent_id,(input_value+1));
    cloneFormProduct(id,$(this).parent().find('input').val());
});

/* REMOVE PRODUCT QUANTITY */
$('table.product button.less').click(function(e){
    e.preventDefault();
    var id = $(this).parents('table.product').attr('data-id');
    var parent_id = $(this).parents('table.product').attr('id');
    if(parent_id == 'product-table-0'){
        var input_value = $(this).parent().find('input').val()*1;
        if(input_value > 1) {
            $(this).parent().find('input').val(input_value-1)
            removeFormProduct(id);
            changeTotalPriceProduct(parent_id,(input_value-1));
        }
    }else{
        var input_value = $(this).parent().find('input').val()*1;
        if(input_value > 0) {
            $(this).parent().find('input').val(input_value-1);
            removeFormProduct(id);
            changeTotalPriceProduct(parent_id,(input_value-1));
        }
    }
});

$('#next_step').click(function(e){

    e.preventDefault();
    //var nameReg = /^[a-zA-Z]$/;
    var birthReg = /^[0-9]\/[0-9]\/[0-9]+$/;
    var birthdayError = false;
    var requireError = false;

    $('.form-product-validator input').each(function(){
        $(this).removeClass('error');
        if(!$(this).hasClass('birthday_selector')){
            if($(this).val() == '') {
                $(this).addClass('error');
                requireError = true;
            }
        }else{
            if($(this).val() != '') {
                var birthDay = $(this).val();
                var array_birthDay = birthDay.split("/");
                birthDay = array_birthDay[2]+'/'+array_birthDay[1]+'/'+array_birthDay[0];
                var DOB = new Date(birthDay);
                var today = new Date();
                var age = today.getTime() - DOB.getTime();
                age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

                if(age < 6 || age >= 16){
                    $(this).addClass('error');
                    birthdayError = true;
                }
            }else{
                $(this).addClass('error');
                requireError = true;
            }
        }
    });
    if(requireError || birthdayError) {
        var html = '';
        if(requireError){
            $('.message ul').html(html+'<li>Veuillez renseigner les champs obligatoires</li>');
            html = $('.message ul').html();
        }
        if(birthdayError){
            $('.message ul').html(html+'<li>Votre enfant doit avoir entre 6  et 16ans pour pouvoir avoir le Yetikids (les enfants de moins de 6ans sont admis gratuitement)</li>');
        }

        $('.message').show();

        $("html, body").animate({ scrollTop: 0 }, "slow");

    }else{
        $('.message ul').html('');
        $('.message').hide();

        jQuery.ajax({
            url : 'http://'+window.location.hostname+'/wp/wp-admin/admin-ajax.php',
            type : 'post',
            data : {
                action: "addItemToCart",
                postdata: $("#form_products").serializeArray()
            },
            success : function( response ) {
                window.location.href = 'http://'+window.location.hostname+'/checkout';
            }
        });

    }
});

/* LIST FUNCTIONS */
function changeTotalPriceProduct(parent_id,qte) {
    var price_uni = $('#'+parent_id).attr('data-price')*1;
    var total = price_uni*qte;
    $('#'+parent_id).find('.amount').html(total);
    changeTotalCart();
}
function changeTotalCart() {
    var amount = 0;
    $('table.product').find('.amount').each(function(){
        amount += $(this).html()*1;
    });
    $('div.total-cart .amount').html(amount);
}

function cloneFormProduct(id,qte){
    var clone = $('.duplicator-form-'+id);
    var afterElement = $('section#form-product-id-'+id).find('div.form-pos').last();
    if(afterElement.length != 0){
        afterElement.after(
            clone.clone().removeClass('duplicator-form duplicator-form-'+id).addClass('form-product-validator form-product-'+id)
        );
    }else{
        clone.after(
            clone.clone().removeClass('duplicator-form duplicator-form-'+id).addClass('form-product-validator form-product-'+id)
        );
    }
}

function removeFormProduct(id){
    $('.form-product-'+id).last().remove();
}

$('body').on('focus',".datepicker", function(){
    $(this).datepicker({
        yearRange: '2001:2017',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy'
    });
});