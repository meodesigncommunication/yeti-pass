$(document).ready(function(){

    resizeMarginContent();
    resizeHeightActivity();
    resizeHeightStep();
});

$(window).resize(function(){
    resizeMarginContent();
    resizeHeightActivity();
    resizeHeightStep();
})

$('div.menu-primary-menu-container ul li a').click(function(){
    $('nav.main').animate({
        width: '0'
    },500,function(){
        $('#background-nav').css('display','none');
    });
});

$('i.nav').click(function(){
    if($(this).hasClass('fa-times')) {
        $('nav.main').animate({
            width: '0'
        },500,function(){
            $('#background-nav').css('display','none');
        });
    }else{
        $('#background-nav').css('display','block');
        if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            $('nav.main').animate({
                width: '70%'
            }, 500);
        }else {
            $('nav.main').animate({
                width: '50%'
            }, 500);
        }
    }
});

function resizeMarginContent() {
    if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        var height_margin = $('header').height()+($('header').height()/2);
        $('div.margin-nav').css('margin-bottom', '30px');
    }else{
        var height_margin = $('header').height()+($('header').height()/2);
    }
    $('div.margin-nav').height(height_margin+'px');
}

function resizeHeightActivity() {
    var width_activity = $('.activities article').width();
    $('.activities article').height(width_activity + 'px');
}

function resizeHeightStep() {
    var width_step = $('.step ul li span.number').width();
    $('.step ul li span.number').height(width_step + 'px');
}