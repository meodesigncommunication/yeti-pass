/**
 * Created by kylemobilia on 11.10.17.
 */
$(function(){

    function resizeHeightSlider() {

        if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            var ratio = 0.8;
        }else{
            var ratio = 1.50256787;
        }

        var width = $('#slider').width();
        var height = width/ratio;

        $('#slider').height(height);

        $('#slider .slide').each(function(){
            $(this).find('.slide-bg-gradient').height(height);
        });
    }

    function resizeHeightDiagSlider() {

        var ratio = 9.61502347;
        var width = $('#slider').width();
        var height = width/ratio;

        $('.triangle').css('border-right-width',width+'px');
        $('.triangle').css('border-top-width',height+'px');
    }

    resizeHeightSlider();
    resizeHeightDiagSlider();

    var $slides = $("#slider .slide");
    //var $slidesTXT = $("#slider-txt .slide-txt");
    var currentSlide = 0;
    var stayTime = 6;
    var slideTime = 2.5;

    TweenLite.set($slides.filter(":gt(0)"), {scale: 1, autoAlpha:0});
    TweenLite.delayedCall(stayTime, nextSlide);

    function nextSlide()
    {
        TweenLite.to( $slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:0} );
        //TweenLite.to( $slidesTXT.eq(currentSlide), slideTime, {autoAlpha:0} );
        currentSlide = ++currentSlide % $slides.length;
        TweenLite.to( $slides.eq(currentSlide), slideTime, {scale: 1, autoAlpha:1} );
        //TweenLite.to( $slidesTXT.eq(currentSlide), slideTime, {autoAlpha:1} );
        TweenLite.delayedCall(stayTime, nextSlide);
    }

    $(window).resize(function(){
        resizeHeightSlider();
        resizeHeightDiagSlider();
    });

});