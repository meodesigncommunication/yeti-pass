<?php
/*
 * Template name: Shop
 */
require_once 'includes/Mobile_Detect.php';

global $post, $woocommerce;

$detect = new Mobile_Detect();

//INIT COUNTER
$count = 0;

// GET PRODUCT IN CART
$cart_items = $woocommerce->cart->get_cart();

// GET ALL PRODUCTS
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'order_by' => 'id',
    'order' => 'ASC'
);
$total = 0;
$products = query_posts($args);
$currency = get_woocommerce_currency();
?>
<?php get_header(); ?>
    <div class="content" role="main" style="margin-bottom: 200px">
        <div class="wrapper">

            <div class="step">
                <ul>
                    <li class="active">
                        <span class="number">
                            <p>1</p>
                        </span>
                        <span>panier</span>
                    </li>
                    <li>
                        <span class="number">
                            <p>2</p>
                        </span>
                        <span>compte et paiement</span>
                    </li>
                    <li>
                        <span class="number">
                            <p>3</p>
                        </span>
                        <span>confirmation</span>
                    </li>
                </ul>
            </div>
            <div class="message">
                <ul class="woocommerce-error"></ul>
            </div>
            <section class="shop">

                <form id="form_products">

                    <div class="head-table">
                        <p>Yetipass</p>
                        <p>Prix</p>
                        <p>Dépôt carte magnétique <sup>2</sup></p>
                        <p>Quantité</p>
                        <p>Total</p>
                    </div>

                    <?php foreach ($products as $product): ?>
                        <?php
                            $woo_product = new WC_Product($product->ID);
                        ?>
                        <?php
                            // CHECK THE CART FOR TAKE THE PRODUCT
                            $quantity = 0;
                            $counter = 0;
                            $sub_total = 0;
                            $variations_cart = array();
                            foreach($cart_items as $cart_item) {
                                if($cart_item['product_id'] == $product->ID) {

                                    $quantity += $cart_item['quantity'];
                                    $sub_total += $cart_item['line_total'];
                                    $total += $sub_total;

                                    if($cart_item['variation']) {
                                        foreach($cart_item['variation'] as $key_var => $variation){
                                            if($cart_item['product_id'] == 12) {
                                                list($last_name, $first_name) = explode(' - ',$variation);
                                                $variations_cart[$counter]['last_name'] = $last_name;
                                                $variations_cart[$counter]['first_name'] = $first_name;
                                            }else{
                                                list($full_name, $birthday) = explode(' | ',$variation);
                                                list($last_name, $first_name) = explode(' - ',$full_name);
                                                $variations_cart[$counter]['last_name'] = $last_name;
                                                $variations_cart[$counter]['first_name'] = $first_name;
                                                $variations_cart[$counter]['birthday'] = $birthday;
                                            }
                                            $counter++;
                                        }
                                    }
                                }
                            }
                        ?>

                        <input type="hidden" name="product_id" value="<?php echo $product->ID ?>" />

                        <table class="product" data-price="<?php echo $woo_product->get_price(); ?>" data-id="<?php echo $product->ID ?>" id="product-table-<?php echo $count ?>">
                            <?php if($detect->isMobile()): ?>
                            <tr class="mobile-table product-name-mobile">
                                <td colspan="3"><?php echo $woo_product->get_title(); ?></td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <?php if(!$detect->isMobile()): ?>
                                <td><?php echo $woo_product->get_short_description(); ?></td>
                                <?php endif; ?>
                                <?php if($woo_product->get_price() == '15'): ?>
                                    <td><?php echo $currency.' '.($woo_product->get_price()-5).'.- <sup>3</sup>'; ?></td>
                                <?php else: ?>
                                    <td><?php echo $currency.' '.($woo_product->get_price()-5).'.-'; ?></td>
                                <?php endif; ?>
                                <td><?php echo $currency ?> 5.-</td>
                                <td class="qte">
                                    <div>
                                        <button class="less">-</button>
                                        <input type="text" name="qte-<?php echo $product->ID ?>" id="qte-<?php echo $product->ID ?>" value="<?php echo $quantity ?>">
                                        <button class="add">+</button>
                                    </div>
                                </td>
                                <?php if(!$detect->isMobile()): ?>
                                <td><?php echo $currency ?> <span class="amount"><?php echo $sub_total ?></span>.-</td>
                                <?php endif; ?>
                            </tr>
                            <?php if($detect->isMobile()): ?>
                            <tr class="mobile-table product-price-mobile">
                                <td colspan="3"><?php echo $currency ?> <span class="amount"><?php echo $sub_total ?></span>.-</td>
                            </tr>
                            <?php endif; ?>
                        </table>

                        <?php if($product->ID == 12): ?>
                            <section id="form-product-id-<?php echo $product->ID ?>" class="form-data-product">
                                <div class="duplicator-form duplicator-form-<?php echo $product->ID ?> form-pos">
                                    <div>
                                        <div><label><?php echo __('[:fr]Nom[:]') ?><sup>*</sup></label><input name="yetipass-last_name" type="text" value="" /></div>
                                        <div><label><?php echo __('[:fr]Prénom[:]') ?><sup>*</sup></label><input name="yetipass-first_name" type="text" value="" /></div>
                                    </div>
                                </div>
                                <?php foreach($variations_cart as $variation): ?>
                                    <div class="form-pos form-product-validator form-product-<?php echo $product->ID ?>">
                                        <div>
                                            <div><label>Nom<sup>*</sup></label><input name="yetipass-last_name" value="<?php echo $variation['last_name'] ?>" type="text"></div>
                                            <div><label>Prénom<sup>*</sup></label><input name="yetipass-first_name" value="<?php echo $variation['first_name'] ?>" type="text"></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </section>
                        <?php else: ?>
                            <section id="form-product-id-<?php echo $product->ID ?>" class="form-data-product-kid">
                                <div class="duplicator-form duplicator-form-<?php echo $product->ID ?> form-pos">
                                    <div>
                                        <div><label><?php echo __('[:fr]Nom[:]') ?><sup>*</sup></label><input name="yetikids-last_name" type="text" value="" /></div>
                                        <div><label><?php echo __('[:fr]Prénom[:]') ?><sup>*</sup></label><input name="yetikids-first_name" type="text" value="" /></div>
                                        <div><label><?php echo __('[:fr]Date de naissance[:]') ?><sup>*</sup></label><input class="datepicker birthday_selector" name="yetikids-birthday" type="text" value="" /><i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach($variations_cart as $variation): ?>
                                    <div class="form-pos form-product-validator form-product-<?php echo $product->ID ?>">
                                        <div>
                                            <div><label>Nom<sup>*</sup></label><input name="yetikids-last_name" value="<?php echo $variation['last_name'] ?>" type="text"></div>
                                            <div><label>Prénom<sup>*</sup></label><input name="yetikids-first_name" value="<?php echo $variation['first_name'] ?>" type="text"></div>
                                            <div><label><?php echo __('[:fr]Date de naissance[:]') ?><sup>*</sup></label><input class="datepicker birthday_selector" name="yetikids-birthday" type="text" value="<?php echo $variation['birthday'] ?>" /><i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </section>
                        <?php endif; ?>

                        <?php $count++; ?>

                    <?php endforeach; ?>

                    <div class="total-cart">
                        <p><?php echo __('[:fr]Total du panier (TTC)[:]') ?></p>
                        <p><?php echo $currency ?> <span class="amount"><?php echo $total ?></span>.-</p>
                    </div>
                    <p class="informations"><?php echo __('[:fr]* Champs obligatoire[:]') ?></p>
                    <p class="informations"><?php echo __('[:fr]2 La Keycard est fournie comme titre de transport. Ce support nécessite une consigne de CHF 5.- remboursable à notre guichet[:]') ?></p>
                    <p class="informations"><?php echo __('[:fr]3 Le tarif enfant à CHF 10.- est applicable uniquement si l’abonnement est contracté avec l’abonnement de l’un des parents[:]') ?></p>
                    <p class="txt-important"><br/>
                        Un abonnement enfant sans l’achat d’un abonnement adulte est disponible pour le prix de CHF 63.-. Veuillez nous contacter pour plus d’information

                    </p>

                    <div class="bottom-panel">
                        <p class="txt-important">
                            &nbsp;
                        </p>
                        <button id="next_step"><?php echo __('[:fr]étape suivante >[:]') ?></button>
                    </div>

                </form>
            </section>

        </div>
    </div>
    <script src="<?php bloginfo('template_directory'); ?>/js/cart.js"></script>
<?php get_footer(); ?>