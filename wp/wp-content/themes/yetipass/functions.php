<?php
/**
 * Functions PHP
 * Autor: MEO design & communication (Kyle Mobilia)
 */

register_nav_menu( 'primary', 'Primary Menu' );

/* Create Custom Post Type Event */

function create_post_types() {

    // Post Type
    $labels = array(
        'name' => 'Evenements',
        'all_items' => 'Tous les événements',
        'singular_name' => 'Event',
        'add_new_item' => 'Ajouter un événement',
        'edit_item' => "Modifier l'événement",
        'menu_name' => 'Evenements'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-calendar-alt',
    );

    register_post_type('events',$args);

}
add_action('init', 'create_post_types');

show_admin_bar(false);

/* FUNCTION WOOCOMMERCE */

/*
 * Get a cart product/item's variation as a key=>value pair
 */
function store_get_cart_product_variations($cart_item) {

    $variations = WC()->cart->get_item_data($cart_item, true);

    /* Explode and trim
    $parts = explode(PHP_EOL, $variations);
    $parts = array_filter($parts);

    // Build a key=>value pair, trim any extra whitespace
    $variations = array();
    foreach($parts as $part) {
        list($key, $value) = explode(':', $part);
        $variations[trim($key)] = trim($value);
    }*/

    return $variations;
}

/* AJAX QUERY */

/*
 * ADD ITEM PAGE CART IN CART
 */
function addItemToCart() {

    global $woocommerce;
    $woocommerce->cart->empty_cart();

    // Init Variables
    $count_product = 0;
    $count_variation = 0;
    $products = array();
    $all_data = $_POST['postdata'];

    foreach($all_data as $data) {
        if($data['name'] == 'product_id') {

            $product_id = $data['value'];
            $products[$count_product]['product_id'] = $product_id;

            $woo_product = new WC_Product($product_id);
            $products[$count_product]['slug'] = $woo_product->get_slug();

            foreach ($all_data as $data){
                if($data['name'] == 'qte-'.$product_id) {
                    $products[$count_product]['quantity'] = $data['value'];
                }
            }

            foreach ($all_data as $data) {
                if($data['name'] == $woo_product->get_slug().'-last_name' && !empty($data['value'])) {
                    $products[$count_product]['variations'][$count_variation]['last_name'] = $data['value'];
                }
                if($data['name'] == $woo_product->get_slug().'-first_name' && !empty($data['value'])) {
                    $products[$count_product]['variations'][$count_variation]['first_name'] = $data['value'];
                    if($woo_product->get_slug() != 'yetikids'){
                        $count_variation++;
                    }
                }
                if($data['name'] == $woo_product->get_slug().'-birthday' && !empty($data['value'])) {
                    $products[$count_product]['variations'][$count_variation]['birthday'] = $data['value'];
                    $count_variation++;
                }
            }

            $count_variation = 0;
            $count_product++;
        }
    }

    foreach($products as $product){
        $count = 1;
        $array = array();
        foreach ($product['variations'] as $variation) {
            if($product['slug'] == 'yetipass'){
                $array[$count.'. Nom complet'] = $variation['last_name'].' - '.$variation['first_name'];
            }else{
                $array[$count.'. Nom complet'] = $variation['last_name'].' - '.$variation['first_name'].' | '.$variation['birthday'];
            }
            $count++;
        }
        $woocommerce->cart->add_to_cart($product['product_id'],$product['quantity'],0,$array);
    }

    die();
}
add_action( 'wp_ajax_addItemToCart', 'addItemToCart' );
add_action( 'wp_ajax_nopriv_addItemToCart', 'addItemToCart' );