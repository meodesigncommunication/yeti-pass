<?php
/*
 * Template name: My Account
 */
?>
<?php get_header(); ?>
    <div class="content" role="main" style="margin-bottom: 200px">
        <div class="wrapper account">
            <h2><?php echo __('[:fr]mon compte[:]') ?><img src="<?php bloginfo('template_directory'); ?>/images/Yeti_account.png" alt="Le Yeti" /></h2>
            <?php echo do_shortcode('[woocommerce_my_account]');  ?>
        </div>
    </div>
<?php get_footer(); ?>