<?php get_header(); ?>
<div class="content" role="main" style="margin-bottom: 200px">
    <div class="wrapper">
        <!-- section -->
        <section>
            <h1><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>
            <?php get_template_part('loop'); ?>
            <?php get_template_part('pagination'); ?>
        </section>
        <!-- /section -->
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>