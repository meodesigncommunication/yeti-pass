        <footer>
            <div class="wrapper">
                <div class="col col-1">
                    <img src="<?php echo get_template_directory_uri() ?>/images/Logo_TCO.svg" alt="Télé-Château-d'Oex" />
                </div>
                <div class="col col-2">
                    <p>
                        <span>Télé-Château-d'Oex</span>
                        <br/>
                        Route de la Ray 30 | Case postale 191
                        <br/>
                        CH-1660 Château-d'Oex
                        <br/>
                        +41 (0) 26 924 67 94
                        <br/>
                        <a href="mailto:info@yetipass.ch">info@yetipass.ch</a>
                    </p>
                </div>
                <div class="col col-3">
                    <img src="<?php echo get_template_directory_uri() ?>/images/Yeti_pouce.png" alt="Le Yeti de la Braye" />
                </div>
            </div>
        </footer>
        <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
        <!-- Google Analytics: change UA-107995365-1 to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-107995365-1','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.10&appId=1157089911102079';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <?php wp_footer(); ?>
    </body>
</html>