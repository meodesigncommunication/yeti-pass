<?php

    // GET METADATA
    $meta_title = (!empty(get_field('meta_title',$post->ID))) ? get_field('meta_title',$post->ID) : '';
    $meta_description = (!empty(get_field('meta_description',$post->ID))) ? get_field('meta_description',$post->ID) : '';
    $meta_keywords = (!empty(get_field('meta_keywords',$post->ID))) ? get_field('meta_keywords',$post->ID) : '';

?>
<!doctype html>
<html class="no-js" lang="fr_ch">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $meta_title ?></title>
    <meta name="description" content="<?php echo $meta_description ?>">
    <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="h_Mha7KbDRKb1eCaH0HhZdIjuFJdm1RSZ1E2Kz_xBRc" />
    <meta property="og:url" content="<?php echo home_url( '/' ) ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Cet hiver, passons plus de temps dans les montagnes enneigées" />
    <meta property="og:description" content="Le YETIPASS offre un accès illimité au domaine de la Braye à Château-d’Oex cette saison 17/18. De la neige naturelle, des pistes impeccables, un parking gratuit à 2 minutes du téléphérique et un restaurant sommital chaleureux, La Braye, c’est le domaine des skieurs et des non-skieurs, il y en a pour tous les goûts. Avec mon code YETI, profitez de l’un des 3 avantages offerts à l’achat d’un YETIPASS." />
    <meta property="og:image" content="<?php echo get_template_directory_uri() ?>/images/YP_elements_facebook_partage.jpg"/>

    <?php  do_action( 'additional_header_metadata' );  ?>
    <!-- Add Favicon + other mobile icon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/favicon.ico" type="image/x-icon" />

    <?php wp_head(); ?>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery-3.2.1.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1675752899332574');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=1675752899332574&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11&appId=576222319110542';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="margin-nav"></div>
    <div id="background-nav"></div>
    <nav class="main">
        <i class="nav fa fa-times" aria-hidden="true"></i>
        <?php wp_nav_menu(); ?>
    </nav>
    <header>
        <div class="wrapper">
            <div>
                <a href="<?php echo home_url( '/' ) ?>" title="<?php echo __('[:fr]Page d\'accueil[:]') ?>">
                    <span>Château-d'oex</span>
                    <span>La Braye</span>
                    <div class="under-logo">Ski - luge - snowpark - ski de fond - balade au sommet - restaurant</div>
                </a>
            </div>
            <div class="group-nav">
                <!--<a href="<?php echo home_url( '/' ) ?>/my-account/" title="<?php echo __('[:fr]Mon compte[:]') ?>"><i class="fa fa-user" aria-hidden="true"></i></a>-->
                <i class="nav fa fa-bars close" aria-hidden="true"></i>
            </div>
        </div>
    </header>




