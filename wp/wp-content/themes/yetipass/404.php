<?php
/*
 * Template name: Shop
 */

global $post, $woocommerce;

//INIT COUNTER
$count = 0;

// GET PRODUCT IN CART
$cart_items = $woocommerce->cart->get_cart();

// GET ALL PRODUCTS
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'order_by' => 'id',
    'order' => 'ASC'
);
$products = query_posts($args);
$currency = get_woocommerce_currency();

?>
<?php get_header(); ?>
    <div class="content" role="main" style="margin-bottom: 200px">
        <div class="wrapper">

            <h1><?php echo __('[fr:]Erreur 404[:]') ?></h1>
            <a href="<?php echo home_url() ?>" title="<?php echo __('[fr:]Retour à la page d\'accueil[:]') ?>"><?php echo __('[fr:]Retour à la page d\'accueil[:]') ?></a>

        </div>
    </div>
    <script src="<?php bloginfo('template_directory'); ?>/js/cart.js"></script>
<?php get_footer(); ?>